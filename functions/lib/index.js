"use strict";
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
Object.defineProperty(exports, "__esModule", { value: true });
const tournamentModule_1 = require("./tournamentModule");
const groupGame_1 = require("./groupGame");
const knockOut_1 = require("./knockOut");
const functions = require(`firebase-functions`);
const express = require('express');
const cors = require('cors');
const app = express();
var request = require("request");
// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
// Expose Express API as a single Cloud Function:
exports.widgets = functions.https.onRequest(app);
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require(`firebase-admin`);
admin.initializeApp();
var userFcmMap = {};
var userNameMap = {};
function yyyymmdd() {
    var now = new Date();
    var mm = now.getMonth() + 1; // getMonth() is zero-based
    var dd = now.getDate();
    return [now.getFullYear() + `-`,
        (mm > 9 ? `` : `0`) + mm + `-`,
        (dd > 9 ? `` : `0`) + dd + `-`,
    ].join(``);
}
;
function getAllUserFcmToken() {
    return new Promise((resolve) => {
        console.log("getAllUserFcmToken called");
        const getUsersItemPromise = admin.database().ref(`/userItems/`).once(`value`);
        return Promise.all([getUsersItemPromise]).then((res) => {
            console.log("getAllUserFcmToken in Promise");
            var users = res[0].val();
            var userKeys = Object.keys(users);
            userKeys.forEach(uKey => {
                if (users[uKey].fcmToken) {
                    userFcmMap[uKey] = users[uKey].fcmToken;
                }
                userNameMap[uKey] = users[uKey].user_name;
            });
            resolve();
        });
    });
}
function sendCreateGameNotify(game) {
    console.log("sendCreateGameNotify called");
    if ((game.tourna_type == "A")) {
        var tokens = [];
        var allUserkey = Object.keys(userFcmMap);
        allUserkey.forEach(key => {
            var fcm = userFcmMap[key];
            if (fcm) {
                tokens.push(fcm);
            }
        });
        console.log("sendCreateGameNotify ready");
        // Notification details.
        const payload = {
            notification: {
                title: `Create New Game`,
                body: `${game.tourna_name} Create, 1st Price : ${game.tourna_first_price}`,
            }
        };
        console.log("sendCreateGameNotify sended");
        if (tokens.length) {
            return admin.messaging().sendToDevice(tokens, payload);
        }
    }
    return;
}
function sendEntryNotifycation(targetId, Title, message) {
    return new Promise((resolve, reject) => {
        console.log("** sendNotification called");
        var prms = admin.database().ref(`/userItems/${targetId}/`).once(`value`);
        Promise.all([prms]).then(res => {
            let targetUser = res[0].val();
            if (targetUser.hasOwnProperty('fcmToken')) {
                console.log("** sendNotification ready : " + targetUser.user_name);
                // Notification details.
                const payload = {
                    notification: {
                        title: `${Title}`,
                        body: `${message}`,
                    }
                };
                console.log("** sendNotification sended : " + targetUser.fcmToken);
                admin.messaging().sendToDevice(targetUser.fcmToken, payload);
                resolve();
                return;
            }
            console.error("** Failed -> hasNotFcm");
            reject('hasNotFcm');
        });
    });
}
function moveToFinishedTournament(code, game, winnerId) {
    var gameKey = game.key;
    game.tourna_state = "Disable";
    game.key_finish = game.key;
    game.champion = winnerId;
    console.log("remove tournakey[" + gameKey + " add finish, champion is : " + winnerId);
    admin.database().ref(`/${code}/finishedTournamentsItems/${gameKey}/`).set(game);
    return admin.database().ref(`/${code}/tournamentsItems/${gameKey}/`).remove();
}
function sendSlackToChannelAndMessage(channel, message) {
    var webHookUrl = "";
    console.log("channel : " + channel + ", msg : " + message);
    webHookUrl = "https://slack.com/api/chat.postMessage?token=xoxp-412860052289-413416051106-427577131589-85529b3bed33a498da8fbfed5d389326&channel=%23" + channel + "&text=" + message + "&username=Dartour-Server&pretty=1";
    return request.post(webHookUrl);
}
function countJoinUserEntryCount(Game) {
    return new Promise((resolve) => {
        console.log("!!!!! start countJoinUserEntryCount");
        var userKeys = Object.keys(Game.join_user);
        // get current state
        var userItemList = [];
        userKeys.forEach(key => {
            userItemList.push(admin.database().ref(`/userItems/${key}/`).once(`value`));
        });
        return Promise.all(userItemList).then(res => {
            var userList = res;
            userList.forEach(pusr => {
                var usr = pusr.val();
                var count = 0;
                var uid = usr.uid;
                if (usr.hasOwnProperty('match_participation')) {
                    count = usr.match_participation + 1;
                    admin.database().ref(`/userItems/${uid}/match_participation`).set(count);
                }
                else {
                    count = 1;
                    admin.database().ref(`/userItems/${uid}/match_participation`).set(count);
                }
            });
            return;
        });
    });
}
function countUpChampionCnt(userKey) {
    return new Promise((resolve) => {
        // get champion user item
        var count = 0;
        var championUser = admin.database().ref(`/userItems/${userKey}/`).once(`value`);
        return Promise.all([championUser]).then(res => {
            var chpUser = res[0].val();
            if (chpUser.hasOwnProperty('match_champion')) {
                count = chpUser.match_champion + 1;
                admin.database().ref(`/userItems/${userKey}/match_champion`).set(count);
            }
            else {
                count = 1;
                admin.database().ref(`/userItems/${userKey}/match_champion`).set(count);
            }
        });
    });
}
function countUpFinalUserCnt(userKey1, userKey2) {
    return new Promise((resolve) => {
        // get champion user item
        var count1 = 0;
        var count2 = 0;
        var fUser1 = admin.database().ref(`/userItems/${userKey1}/`).once(`value`);
        var fUser2 = admin.database().ref(`/userItems/${userKey2}/`).once(`value`);
        return Promise.all([fUser1, fUser2]).then(res => {
            var finalUser1 = res[0].val();
            var finalUser2 = res[1].val();
            if (finalUser1.hasOwnProperty('match_final_participation')) {
                count1 = finalUser1.match_final_participation + 1;
                admin.database().ref(`/userItems/${userKey1}/match_final_participation`).set(count1);
            }
            else {
                count1 = 1;
                admin.database().ref(`/userItems/${userKey1}/match_final_participation`).set(count1);
            }
            if (finalUser2.hasOwnProperty('match_final_participation')) {
                count2 = finalUser2.match_final_participation + 1;
                admin.database().ref(`/userItems/${userKey2}/match_final_participation`).set(count2);
            }
            else {
                count2 = 1;
                admin.database().ref(`/userItems/${userKey2}/match_final_participation`).set(count2);
            }
        });
    });
}
function countUpSemiFinalUserCnt(userKey1, userKey2) {
    return new Promise((resolve) => {
        // get champion user item
        var count1 = 0;
        var count2 = 0;
        var fUser1 = admin.database().ref(`/userItems/${userKey1}/`).once(`value`);
        var fUser2 = admin.database().ref(`/userItems/${userKey2}/`).once(`value`);
        return Promise.all([fUser1, fUser2]).then(res => {
            var finalUser1 = res[0].val();
            var finalUser2 = res[1].val();
            if (finalUser1.hasOwnProperty('match_semifinal_participation')) {
                count1 = finalUser1.match_semifinal_participation + 1;
                admin.database().ref(`/userItems/${userKey1}/match_semifinal_participation`).set(count1);
            }
            else {
                count1 = 1;
                admin.database().ref(`/userItems/${userKey1}/match_semifinal_participation`).set(count1);
            }
            if (finalUser2.hasOwnProperty('match_semifinal_participation')) {
                count2 = finalUser2.match_semifinal_participation + 1;
                admin.database().ref(`/userItems/${userKey2}/match_semifinal_participation`).set(count2);
            }
            else {
                count2 = 1;
                admin.database().ref(`/userItems/${userKey2}/match_semifinal_participation`).set(count2);
            }
        });
    });
}
function submitGroupGameResult(schedule) {
    return new Promise((resolve) => {
        var scheduleKey = Object.keys(schedule);
        scheduleKey.forEach(num => {
            var score1 = String(schedule[num].result).split(':')[0];
            var score2 = String(schedule[num].result).split(':')[1];
            var user1 = schedule[num].user1;
            var user2 = schedule[num].user2;
            if (score1 > score2) {
                admin.database().ref(`/userItems/${user1}/match_win_by/`).push(user2);
                admin.database().ref(`/userItems/${user2}/match_lose_by/`).push(user1);
            }
            else {
                admin.database().ref(`/userItems/${user1}/match_lose_by/`).push(user2);
                admin.database().ref(`/userItems/${user2}/match_win_by/`).push(user1);
            }
        });
    });
}
function setKnockoutResult(winner, loser) {
    return new Promise((resolve) => {
        admin.database().ref(`/userItems/${winner}/match_win_by/`).push(loser);
        admin.database().ref(`/userItems/${loser}/match_lose_by/`).push(winner);
    });
}
function reviseWinnerCase(winner, loser, winnersLoserList, winnersLoserListKey) {
    // remove once loserById in Winner's Loser List
    winnersLoserListKey.forEach(key => {
        if (winnersLoserList[key] == loser) {
            admin.database().ref(`/userItems/${winner}/match_lose_by/${key}/`).remove();
            return;
        }
    });
}
function reviseLoserCase(winner, loser, losersWinnerList, losersWinnerListKey) {
    // remove once winnerById in Loser's Winner List
    losersWinnerListKey.forEach(key2 => {
        if (losersWinnerList[key2] == winner) {
            admin.database().ref(`/userItems/${loser}/match_win_by/${key2}/`).remove();
            return;
        }
    });
}
function reviseKnockoutResult(winner, loser) {
    return new Promise((resolve) => {
        admin.database().ref(`/userItems/${winner}/match_win_by/`).push(loser);
        admin.database().ref(`/userItems/${loser}/match_lose_by/`).push(winner);
        var loserListOfWinner = admin.database().ref(`/userItems/${winner}/match_lose_by/`).once(`value`);
        var winnerListOfLoser = admin.database().ref(`/userItems/${loser}/match_win_by/`).once(`value`);
        return Promise.all([winnerListOfLoser, loserListOfWinner]).then(res => {
            var winnersLoserList = res[1].val();
            var winnersLoserListKey = Object.keys(winnersLoserList);
            reviseWinnerCase(winner, loser, winnersLoserList, winnersLoserListKey);
            var losersWinnerList = res[0].val();
            var losersWinnerListKey = Object.keys(losersWinnerList);
            reviseLoserCase(winner, loser, losersWinnerList, losersWinnerListKey);
        });
    });
}
exports.discoverFinishedGroupGame = functions.database.ref(`/{code}/tournamentsItems/{token}/round_robin/{robin_token}/scheduleProgress/`).onUpdate((game, context) => {
    console.log("!!!!! discover Finished GroupGame");
    var token = context.params.token;
    var code = context.params.code;
    var robin_token = context.params.robin_token;
    var schedulePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/round_robin/${robin_token}/scheduleProgress/`).once(`value`);
    var robinPromise = admin.database().ref(`/${code}/tournamentsItems/${token}/round_robin/${robin_token}/scheduler/`).once(`value`);
    return Promise.all([schedulePromise, robinPromise]).then((res) => {
        var progress = res[0].val();
        var scheduler = res[1].val();
        if (Number(progress) == 100) {
            submitGroupGameResult(scheduler);
        }
    });
});
exports.discoverGameStarted = functions.database.ref(`/{code}/tournamentsItems/{token}/knock_out/`).onCreate((game, context) => {
    console.log("!!!!! started game discover");
    var token = context.params.token;
    var code = context.params.code;
    var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/`).once(`value`);
    return Promise.all([GamePromise]).then((res) => {
        var createGame = res[0].val();
        countJoinUserEntryCount(createGame).then(() => { return; });
    });
});
// handle for new game
exports.newGameCreate = functions.database.ref(`/{code}/tournamentsItems/{token}/`).onCreate((game, context) => {
    console.log("newGameCreate discover");
    var token = context.params.token;
    var code = context.params.code;
    var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/`).once(`value`);
    return Promise.all([GamePromise]).then((res) => {
        var createGame = res[0].val();
        createGame.key = token;
        var optimizeTournaName = createGame.tourna_name;
        var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
        var tournaName = optimizeTournaName.replace(regExp, " ");
        if (createGame.tourna_many == "") {
            createGame.tourna_many = "32";
        }
        countJoinUserEntryCount(createGame);
        admin.database().ref(`${code}/tournamentsItems/${token}/`).update(createGame);
        admin.database().ref(`${code}/searchTournament/${tournaName}/`).set(token);
        getAllUserFcmToken().then(() => {
            sendCreateGameNotify(createGame);
            return console.log("newGameCreate ended");
        });
    });
});
// handle for new game
exports.finishedGameCreate = functions.database.ref(`/{code}/finishedTournamentsItems/{token}/`).onCreate((game, context) => {
    console.log("new finished GameCreate discover");
    var token = context.params.token;
    var code = context.params.code;
    var GamePromise = admin.database().ref(`/${code}/finishedTournamentsItems/${token}/`).once(`value`);
    return Promise.all([GamePromise]).then((res) => {
        var createGame = res[0].val();
        createGame.key_finish = token;
        return admin.database().ref(`/${code}/finishedTournamentsItems/${token}/`).update(createGame);
    });
});
function sendCreateGroupGameNotifyToGroupUser(game, groupGame) {
    console.log("sendCreateGroupGameNotifyToGroupUser called");
    if ((game.tourna_type == "A")) {
        var tokens = [];
        var Userkey = Object.keys(groupGame.attendedMembers);
        Userkey.forEach(key => {
            var fcm = userFcmMap[groupGame.attendedMembers[key]];
            if (fcm) {
                tokens.push(fcm);
            }
        });
        console.log("sendCreateGroupGameNotifyToGroupUser ready");
        // Notification details.
        const payload = {
            notification: {
                title: `Your Board Number ${groupGame.boardNumber}`,
                body: `${game.tourna_name} Started Round Robin, Good luck`,
            }
        };
        console.log("sendCreateGroupGameNotifyToGroupUser sended");
        if (tokens.length) {
            return admin.messaging().sendToDevice(tokens, payload);
        }
    }
    return;
}
// handle for create group game
exports.newGroupCreate = functions.database.ref(`/{code}/tournamentsItems/{token}/round_robin/{game_token}/`).onCreate((game, context) => {
    console.log("newGroupCreate discover");
    var token = context.params.token;
    var code = context.params.code;
    var game_token = context.params.game_token;
    var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/`).once(`value`);
    var GroupGamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/round_robin/${game_token}/`).once(`value`);
    return Promise.all([GamePromise, GroupGamePromise]).then((res) => {
        var createGame = res[0].val();
        var createGroupGame = res[1].val();
        console.log("newGroupCreate : " + createGroupGame);
        getAllUserFcmToken().then(() => {
            sendCreateGroupGameNotifyToGroupUser(createGame, createGroupGame);
            sendScheduleGroupGameNotifyToScheduleUser(createGame, createGroupGame);
        });
        console.log("newGroupCreate ended");
    });
});
function sendScheduleGroupGameNotifyToScheduleUser(game, groupGame) {
    console.log("sendScheduleGroupGameNotifyToScheduleUser called");
    if ((game.tourna_type == "A")) {
        var tokens = [];
        var schedule = groupGame.currentScheduleNumber;
        var maxSchedule = groupGame.scheduleMaxNumber;
        var currentSchedule = groupGame.scheduler[schedule];
        var showCurrentSchedule = String(Number(schedule) + 1);
        var user1 = currentSchedule["user1"];
        console.log("user1 : " + user1);
        var user2 = currentSchedule["user2"];
        console.log("user2 : " + user2);
        var userName1 = userNameMap[user1];
        var userName2 = userNameMap[user2];
        var fcm1 = userFcmMap[user1];
        if (fcm1) {
            tokens.push(fcm1);
        }
        var fcm2 = userFcmMap[user2];
        if (fcm2) {
            tokens.push(fcm2);
        }
        // Notification details.
        const payload = {
            notification: {
                title: `Match[Number:${showCurrentSchedule}] : ${userName1} vs ${userName2}`,
                body: `Started your game, Go to ${groupGame.boardNumber} Board`,
            }
        };
        if (tokens.length) {
            return admin.messaging().sendToDevice(tokens, payload);
        }
    }
    return;
}
// handle for schedule group game
exports.updateGroupCreate = functions.database.ref(`/{code}/tournamentsItems/{token}/round_robin/{game_token}/scheduler/`).onUpdate((game, context) => {
    console.log("updateGroupCreate discover");
    var token = context.params.token;
    var code = context.params.code;
    var game_token = context.params.game_token;
    var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/`).once(`value`);
    var GroupGamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/round_robin/${game_token}/`).once(`value`);
    return Promise.all([GamePromise, GroupGamePromise]).then((res) => {
        var createGame = res[0].val();
        var createGroupGame = res[1].val();
        getAllUserFcmToken().then(() => { sendScheduleGroupGameNotifyToScheduleUser(createGame, createGroupGame); });
    });
});
function sendWinnerGroupGameNotifyToGroupUser(game, groupGame) {
    console.log("sendWinnerGroupGameNotifyToGroupUser called");
    var tokens = [];
    var winner = groupGame.winners;
    var user1 = winner["0"];
    console.log("user1 : " + user1);
    var user2 = winner["1"];
    console.log("user2 : " + user2);
    var userName1 = userNameMap[user1];
    var userName2 = userNameMap[user2];
    var Userkey = Object.keys(groupGame.attendedMembers);
    Userkey.forEach(key => {
        var fcm = userFcmMap[groupGame.attendedMembers[key]];
        if (fcm) {
            tokens.push(fcm);
        }
    });
    // Notification details.
    const payload = {
        notification: {
            title: `Winner of ${groupGame.boardNumber} Board`,
            body: `1st : ${userName1}, 2nd : ${userName2}`,
        }
    };
    if (tokens.length) {
        return admin.messaging().sendToDevice(tokens, payload);
    }
    return;
}
// handle for end group game
exports.GroupWinnerCreate = functions.database.ref(`/{code}/tournamentsItems/{token}/round_robin/{game_token}/winners/`).onCreate((game, context) => {
    console.log("GroupWinnerCreate discover");
    var token = context.params.token;
    var code = context.params.code;
    var game_token = context.params.game_token;
    var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/`).once(`value`);
    var GroupGamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/round_robin/${game_token}/`).once(`value`);
    return Promise.all([GamePromise, GroupGamePromise]).then((res) => {
        var createGame = res[0].val();
        var createGroupGame = res[1].val();
        getAllUserFcmToken().then(() => { sendWinnerGroupGameNotifyToGroupUser(createGame, createGroupGame); });
    });
});
function sendKnocoutGameNotifyToKnocoutUser(round, user1, user2, game) {
    console.log("sendWinnerGroupGameNotifyToGroupUser called");
    if ((game.tourna_type == "A")) {
        var tokens = [];
        var payload;
        if (round != "Champion") {
            var userName1 = userNameMap[user1];
            var userName2 = userNameMap[user2];
            var fcm1 = userFcmMap[user1];
            if (fcm1) {
                tokens.push(fcm1);
            }
            var fcm2 = userFcmMap[user2];
            if (fcm2) {
                tokens.push(fcm2);
            }
            // Notification details.
            payload =
                {
                    notification: {
                        title: `Started ${round}`,
                        body: `${userName1} vs ${userName2}`,
                    }
                };
        }
        else {
            var userName1 = userNameMap[user1];
            var Userkey = Object.keys(userFcmMap);
            Userkey.forEach(key => {
                var fcm = userFcmMap[key];
                if (fcm) {
                    tokens.push(fcm);
                }
            });
            payload =
                {
                    notification: {
                        title: `${userName1} is Chompion of ${game.tourna_name}`,
                        body: `!! Congratulation !!`,
                    }
                };
        }
        if (tokens.length) {
            return admin.messaging().sendToDevice(tokens, payload);
        }
    }
    return;
}
// handle for knockout game
exports.KnockOutUserCreate = functions.database.ref(`/{code}/tournamentsItems/{token}/knock_out/{knockoutKey}/{depth}/{user}/userKey/`).onCreate((game, context) => {
    console.log("KnockOutUserCreate discover");
    var token = context.params.token;
    var code = context.params.code;
    var knockoutKey = context.params.knockoutKey;
    var depth = context.params.depth;
    var user = context.params.user;
    var userNumber = Number(user);
    var findUser;
    if (!(userNumber % 2)) {
        findUser = userNumber + 1;
    }
    else {
        findUser = userNumber - 1;
    }
    findUser = String(findUser);
    console.log("update user number : " + userNumber + ", find User number : " + findUser);
    var oGamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/`).once(`value`);
    var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${token}/knock_out/${knockoutKey}/`).once(`value`);
    var UserPromise = admin.database().ref(`/${code}/tournamentsItems/${token}/knock_out/${knockoutKey}/${depth}/${user}/`).once(`value`);
    var findUserPromise = admin.database().ref(`/${code}/tournamentsItems/${token}/knock_out/${knockoutKey}/${depth}/${findUser}/`).once(`value`);
    return Promise.all([GamePromise, UserPromise, findUserPromise, oGamePromise, depth]).then((res) => {
        var knockGame = res[0].val();
        var tUser = res[1].val();
        var findTargetUser = res[2].val();
        var gameObject = res[3].val();
        var depthVar = Number(res[4]) + 1;
        var depthCal = (Object.keys(knockGame).length) - (depthVar);
        var roundCount = Math.pow(2, depthCal);
        var cmdRound = "";
        var fKey = "userKey";
        var findUserKeys;
        if (findTargetUser != null) {
            findUserKeys = Object.keys(findTargetUser);
        }
        console.log("total count : " + Object.keys(knockGame).length + " , depth : " + depthVar + " , depthCal : " + depthCal);
        if (roundCount == 1) {
            cmdRound = "Champion";
            gameObject['champion'] = tUser.userKey;
            gameObject['tourna_state'] = "Disable";
            countUpChampionCnt(tUser.userKey).then(() => {
                getAllUserFcmToken().then(() => { sendKnocoutGameNotifyToKnocoutUser(cmdRound, tUser.userKey, tUser.userKey, gameObject); });
            });
        }
        else if (roundCount == 2) {
            cmdRound = "FinalRound";
            if (findTargetUser.hasOwnProperty('userKey')) {
                countUpFinalUserCnt(tUser.userKey, findTargetUser.userKey).then(() => {
                    getAllUserFcmToken().then(() => { sendKnocoutGameNotifyToKnocoutUser(cmdRound, tUser.userKey, findTargetUser.userKey, gameObject); });
                });
            }
        }
        else if (roundCount == 4) {
            cmdRound = "SemiFinalRound";
            if (findTargetUser.hasOwnProperty('userKey')) {
                countUpFinalUserCnt(tUser.userKey, findTargetUser.userKey).then(() => {
                    getAllUserFcmToken().then(() => { sendKnocoutGameNotifyToKnocoutUser(cmdRound, tUser.userKey, findTargetUser.userKey, gameObject); });
                });
            }
        }
        else {
            cmdRound = "Round " + roundCount;
            if (findTargetUser.hasOwnProperty('userKey')) {
                getAllUserFcmToken().then(() => { sendKnocoutGameNotifyToKnocoutUser(cmdRound, tUser.userKey, findTargetUser.userKey, gameObject); });
            }
        }
    });
});
// handle, update game
exports.discoverJoinUser = functions.database.ref(`/{code}/tournamentsItems/{token}/join_user/{key}`).onCreate((change, context) => {
    console.log("!! discover JoinUser");
    const token = context.params.token;
    const code = context.params.code;
    const key = context.params.key;
    const JoinUserKey = admin.database().ref(`/${code}/tournamentsItems/${token}/entry_state/${key}/`).set(0);
});
// welcome new user
exports.newGroupCreate = functions.database.ref(`/userItems/{userUid}/`).onCreate((game, context) => {
    console.log("newGroupCreate discover");
    var uid = context.params.userUid;
    var UserPromise = admin.database().ref(`/userItems/${uid}/`).once(`value`);
    return Promise.all([UserPromise]).then((res) => {
        var user = res[0].val();
        console.log("Create New User : " + user.email);
        return sendSlackToChannelAndMessage('dartour-account', "New user comming email (" + user.email + "), name (" + user.user_name + ")");
    });
});
function findOwnerIdInOpenGameList(uId, code) {
    return new Promise((resolve, reject) => {
        var GamePromise = admin.database().ref(`/${code}/tournamentsItems/`).once(`value`);
        return Promise.all([GamePromise]).then((res) => {
            var gameList = res[0].val();
            if (gameList == null)
                reject();
            var gameKeys = Object.keys(gameList);
            gameKeys.forEach(key => {
                if (gameList[key].owner_id == uId) {
                    resolve(key);
                }
            });
            reject();
        });
    });
}
function findOpenGame(gId, code) {
    return new Promise((resolve, reject) => {
        var GamePromise = admin.database().ref(`/${code}/tournamentsItems/`).once(`value`);
        return Promise.all([GamePromise]).then((res) => {
            var gameList = res[0].val();
            if (gameList.hasOwnProperty(gId)) {
                resolve();
            }
            else {
                reject();
            }
        });
    });
}
function removeGame(gId, code) {
    return new Promise((resolve, reject) => {
        var GamePromise = admin.database().ref(`/${code}/tournamentsItems/`).once(`value`);
        var TargetGamePromise = admin.database().ref(`/${code}/tournamentsItems/${gId}/`).once(`value`);
        return Promise.all([GamePromise, TargetGamePromise]).then((res) => {
            var gameList = res[0].val();
            var gameName = "";
            if (gameList.hasOwnProperty(gId)) {
                gameName = res[1].val().tourna_name;
                var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
                var tournaName = gameName.replace(regExp, " ");
                admin.database().ref(`/${code}/searchTournament/${tournaName}/`).remove();
                admin.database().ref(`/${code}/tournamentsItems/${gId}/`).remove();
                resolve();
            }
            else {
                reject();
            }
        });
    });
}
function createTestTournament(uId, count) {
    return new Promise((resolve, reject) => {
        var UserPromise = admin.database().ref(`/userItems/${uId}`).once(`value`);
        return Promise.all([UserPromise]).then((res) => {
            var user = res[0].val();
            if (user.company == "master") {
                let pushtesttourna = {
                    "key": "",
                    "key_finish": "",
                    "tourna_name": "선착순 " + count + " 토너먼트",
                    "tourna_time": "",
                    "tourna_where_shop_name": "경기도 일산 킨텍스 1층",
                    "tourna_where_shop_addr": "테스트입니다. 가지마세요",
                    "tourna_cost": "35000",
                    "tourna_many": 40,
                    "tourna_first_price": "200000",
                    "tourna_second_price": "100000",
                    "tourna_third_price": "20000",
                    "tourna_phone_major": "00199238882",
                    "tourna_phone_minor": "카카오톡 nononon",
                    "tourna_bank": "기업은행",
                    "tourna_bank_addr": "1238889191238",
                    "tourna_accout": user.user_name,
                    "tourna_state": "Active",
                    "owner_id": uId,
                    "join_user_len": 0,
                    "tourna_date": {
                        "day": 10,
                        "hour": 14,
                        "month": 9,
                        "year": 2022
                    }
                };
                admin.database().ref(`/test/tournamentsItems/TeStToUrNaMenT${count}/`).set(pushtesttourna);
                let joinTestUser = [
                    "SOitkZtlKyXFNKVlCnvEuT02bBt2",
                    "nti6wWILQKP8IMWxC5a5LFaU0Hq2",
                    "ZsCtU4RDeIWi8XipRgzjHcHHL0J2",
                    "hxv9jYOvaEX9jHDcF3PXi6EXU3k2",
                    "8Z6qpDiCTATdZB4C1MVZsIT0O823",
                    "vzJNcywCWxeLiqPI5JG92KiLufR2",
                    "LvrHtGiRGAUG3gtHwCLGubDytex2",
                    "GS1Ui7qmfuNKMt3yK40xciwZW9u1",
                    "4GEPZ6469bW0NlE7sTLSZ8kln6v2",
                    "buLTiIKXiCb634lZq5n8lgIXo1v2",
                    "LtXCfOjzdFMhpUYxYqWPGjHMXV42",
                    "jsKigH2PrWMbW2FFQt5TiaKtHjB3",
                    "T7Q3zz2FQLX4Q5aB8JWSli1hryl1",
                    "Vtgazlg55TWTnDARX3TCAww1tPi2",
                    "V3Uo9LQhbPaqxsQ0ZAdExtQGail1"
                ];
                joinTestUser.forEach((userId, index) => {
                    admin.database().ref(`/test/tournamentsItems/TeStToUrNaMenT${count}/join_user/`).push(userId);
                });
                resolve();
            }
            else {
                reject();
            }
        });
    });
}
function removeEntryUser(uid, gid, code) {
    return new Promise((resolve, reject) => {
        console.log("removeEntryUser! : " + uid + ", tournaID : " + gid);
        var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${gid}/`).once(`value`);
        var UserItem = admin.database().ref(`/userItems/${uid}`).once(`value`);
        return Promise.all([GamePromise, UserItem]).then((result) => {
            var createGame = result[0].val();
            var user_item = result[1].val();
            var join_user_key = Object.keys(createGame.join_user);
            var join_user_id_map = {};
            join_user_key.forEach((k) => {
                join_user_id_map[createGame.join_user[k]] = k;
            });
            console.log("join user : " + Object.keys(join_user_id_map));
            if (join_user_id_map.hasOwnProperty(uid)) {
                console.log("do remove user!");
                var targetKey = join_user_id_map[uid];
                admin.database().ref(`/${code}/tournamentsItems/${gid}/join_user/${targetKey}`).remove();
                admin.database().ref(`/${code}/tournamentsItems/${gid}/entry_state/${targetKey}`).remove();
                return resolve();
            }
            reject(400);
        });
    });
}
function finishedTournaments(tournaId, winnerId, code) {
    return new Promise((resolve, reject) => {
        console.log("finished tournaments! : " + tournaId);
        var GamePromise = admin.database().ref(`/${code}/tournamentsItems/${tournaId}/`).once(`value`);
        return Promise.all([GamePromise]).then((result) => {
            var game = result[0].val();
            game.champion = winnerId;
            if (game) {
                moveToFinishedTournament(code, game, winnerId);
                resolve();
            }
            else {
                reject();
            }
        });
    });
}
function codeItemCopy(code, where) {
    return new Promise((resolve) => {
        // var GamePromise = admin.database().ref(`/${code}/`).once(`value`);
        // return Promise.all([GamePromise]).then(res=>{
        //   admin.database().ref(`/${where}/`).update(res[0].val());
        //   resolve();
        // });
        resolve();
    });
}
function searchTournament(code, itemName) {
    return new Promise((resolve, reject) => {
        var searchList = admin.database().ref(`${code}/searchTournament/`).once(`value`);
        var gameList = admin.database().ref(`${code}/tournamentsItems/`).once(`value`);
        return Promise.all([searchList, gameList]).then(res => {
            var list = res[0].val();
            var openList = res[1].val();
            if (list.hasOwnProperty(itemName)) {
                resolve(openList[list[itemName]]);
            }
            else {
                reject();
            }
        });
    });
}
function searchCloseTournament(code, itemName) {
    return new Promise((resolve, reject) => {
        var searchList = admin.database().ref(`${code}/searchTournament/`).once(`value`);
        var gameList = admin.database().ref(`${code}/finishedTournamentsItems/`).once(`value`);
        return Promise.all([searchList, gameList]).then(res => {
            var list = res[0].val();
            var openList = res[1].val();
            if (list.hasOwnProperty(itemName)) {
                resolve(openList[list[itemName]]);
            }
            else {
                reject();
            }
        });
    });
}
function setEntryState(code, tournaKey, uId, value) {
    admin.database().ref(`/${code}/tournamentsItems/${tournaKey}/entry_state/${uId}/`).set(value);
}
exports.restAPI = functions.https.onRequest((req, res) => {
    //set JSON content type and CORS headers for the response
    res.header('Content-Type', 'application/json');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    const tournaModule = new tournamentModule_1.TournamentModule(admin);
    const groupGameModule = new groupGame_1.GroupGameModule(admin);
    const knockOutGameModule = new knockOut_1.KnockOutGameModule(admin);
    var items = req.url.split("/");
    var requestHandle = items[1];
    var code = items[2];
    console.log("request handler name : " + requestHandle);
    if (requestHandle == 'isHasGame') {
        var searchId = items[3];
        findOwnerIdInOpenGameList(searchId, code).then((key) => {
            return res.status(200).send({ res: 'true', id: key });
        }).catch(() => {
            return res.status(200).send({ res: 'false' });
        });
    }
    else if (requestHandle == 'isOpenGame') {
        var searchGameId = items[3];
        console.log("search id : " + searchGameId);
        findOpenGame(searchGameId, code).then(() => {
            return res.status(200).send({ res: 'true' });
        }).catch(() => {
            console.warn("client try attach finished tournament, reject!");
            return res.status(200).send({ res: 'false' });
        });
    }
    else if (requestHandle == 'removeGame') {
        var removeGameId = items[3];
        console.log("remove game : " + removeGameId);
        removeGame(removeGameId, code).then(() => {
            return res.status(200).send({ res: 'true' });
        }).catch(() => {
            console.warn("client try attach finished tournament, reject!");
            return res.status(200).send({ res: 'false' });
        });
    }
    else if (requestHandle == 'createTestTournament') {
        var uid = items[3];
        var many = items[4];
        console.log("createTestTournament, join count : " + many);
        createTestTournament(uid, many).then(() => {
            return res.status(200).send({ res: 'complete' });
        }).catch(() => {
            return res.status(200).send({ res: 'is not master' });
        });
    }
    else if (requestHandle == 'removeEntryUser') {
        var uid = items[3];
        var gid = items[4];
        console.log("removeEntryUser");
        removeEntryUser(uid, gid, code).then(() => {
            return res.status(200).send({ res: 'true' });
        }).catch((code) => {
            return res.status(code).send({ res: 'failed' });
        });
    }
    else if (requestHandle == 'finishedTournament') {
        var tournaId = items[3];
        var winnerId = items[4];
        console.log("finishedTournament");
        finishedTournaments(tournaId, winnerId, code).then(() => {
            return res.status(200).send({ res: 'true' });
        }).catch((code) => {
            return res.status(code).send({ res: 'failed' });
        });
    }
    else if (requestHandle == 'codeItemCopy') {
        var where = items[3];
        console.log("codeItemCopy : " + code + ", to -> " + where);
        codeItemCopy(code, where).then(() => {
            return res.status(200).send({ res: 'success' });
        }).catch(() => {
            return res.status(200).send({ res: 'is not master' });
        });
    }
    else if (requestHandle == 'searchTournament') {
        var itemName = req.body.data;
        console.log("search tournament name : " + itemName);
        var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
        var tournaName = itemName.replace(regExp, " ");
        searchTournament(code, tournaName).then((item) => {
            return res.status(200).send({ res: 'success', data: item });
        }).catch(() => {
            return res.status(200).send({ res: 'cannot found' });
        });
    }
    else if (requestHandle == 'searchCloseTournament') {
        var itemName = req.body.data;
        var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
        var tournaName = itemName.replace(regExp, " ");
        console.log("search close tournament name : " + itemName);
        searchCloseTournament(code, tournaName).then((item) => {
            return res.status(200).send({ res: 'success', data: item });
        }).catch(() => {
            return res.status(200).send({ res: 'cannot found' });
        });
    }
    else if (requestHandle == 'setKnockoutResult') {
        var winnerId = req.body.winner;
        var loserId = req.body.loser;
        console.log("setKnockoutResult : " + winnerId + ", " + loserId);
        setKnockoutResult(winnerId, loserId).then((item) => {
            return res.status(200).send({ res: 'success', data: item });
        }).catch(() => {
            return res.status(200).send({ res: 'cannot found' });
        });
    }
    else if (requestHandle == 'reviseKnockoutResult') {
        var winnerId = req.body.winner;
        var loserId = req.body.loser;
        console.log("reviseKnockoutResult : " + winnerId + ", " + loserId);
        reviseKnockoutResult(winnerId, loserId).then((item) => {
            return res.status(200).send({ res: 'success', data: item });
        }).catch(() => {
            return res.status(200).send({ res: 'cannot found' });
        });
    }
    else if (requestHandle == 'requestStartTournament') {
        var requestItems = req.body.tournaData;
        console.log("requestCreatNewGame : " + JSON.stringify(requestItems));
        tournaModule.setTournamnetFlow(code, requestItems)
            .then((result) => {
            console.log("requestCreatNewGame Success");
            return res.status(200).send({ res: 'success', data: result });
        })
            .catch((error) => {
            console.log("requestCreatNewGame Error");
            return res.status(200).send({ res: 'failed', data: error });
        });
    }
    else if (requestHandle == 'requestSetGroupGameScore') {
        var requestItems = req.body.scoreData;
        console.log("requestSetGroupGameScore : " + JSON.stringify(requestItems));
        groupGameModule.receiveData(code, requestItems)
            .then((result) => {
            console.log("requestSetGroupGameScore Success");
            return res.status(200).send({ res: 'success', data: result });
        })
            .catch((error) => {
            console.log("requestSetGroupGameScore Error");
            return res.status(200).send({ res: 'failed', data: error });
        });
    }
    else if (requestHandle == 'setGroupGameScoreByArrow') {
        var resultIndex = req.body.arrowItems.resultIndex;
        var arrow = req.body.arrowItems.arrow;
        console.log("setGroupGameScoreByArrow : " + resultIndex + arrow);
        groupGameModule.clickReviseResultButton(code, resultIndex, arrow)
            .then((result) => {
            console.log("setGroupGameScoreByArrow Success");
            return res.status(200).send({ res: 'success', data: result });
        })
            .catch((error) => {
            console.log("setGroupGameScoreByArrow Error");
            return res.status(200).send({ res: 'failed', data: error });
        });
    }
    else if (requestHandle == 'setKnockOutScore') {
        var requestItems = req.body.scoreData;
        console.log("setKnockOutScore : " + JSON.stringify(requestItems));
        knockOutGameModule.receiveData(code, requestItems)
            .then((result) => {
            console.log("setKnockOutScore Success");
            return res.status(200).send({ res: 'success', data: result });
        })
            .catch((error) => {
            console.log("setKnockOutScore Error");
            return res.status(200).send({ res: 'failed', data: error });
        });
    }
    else if (requestHandle == 'sendEntryNotifycation') {
        var requestItems = req.body.notifiData;
        setEntryState(code, requestItems.tournaKey, requestItems.setValueTargetId, requestItems.val);
        console.log("sendNotification : " + JSON.stringify(requestItems));
        sendEntryNotifycation(requestItems.targetID, requestItems.title, requestItems.message)
            .then((result) => {
            console.log("sendNotification Success");
            return res.status(200).send({ res: 'success', data: result });
        })
            .catch((error) => {
            console.log("sendNotification Error");
            return res.status(200).send({ res: 'failed', data: error });
        });
    }
    else {
        return res.status(333).send({ res: "wrong requestHandler" });
    }
});
//# sourceMappingURL=index.js.map