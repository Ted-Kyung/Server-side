"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TournaItem {
    constructor() {
        this.key = "";
        this.key_finish = "";
        this.tourna_name = "";
        this.tourna_date = "";
        this.tourna_time = "";
        this.tourna_where_shop_name = "";
        this.tourna_where_shop_addr = "";
        this.tourna_cost = "";
        this.tourna_many = "";
        this.tourna_type = "";
        this.tourna_first_price = "";
        this.tourna_second_price = "";
        this.tourna_third_price = "";
        this.tourna_phone_major = "";
        this.tourna_phone_minor = "";
        this.tourna_bank = "";
        this.tourna_bank_addr = "";
        this.tourna_accout = "";
        this.tourna_state = "";
        this.owner_id = "";
        this.handicap = false;
        this.roundRobinType = "";
        this.knockOutType = "";
        this.join_user = new Map();
        this.join_user_len = 0;
        this.round_robin = new Map();
        this.knock_out = new Map();
        this.double_eli = new Map();
        this.double_eli_final = new Map();
    }
}
exports.TournaItem = TournaItem;
//# sourceMappingURL=tournaItem.js.map