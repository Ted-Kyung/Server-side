"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class UserItem {
    constructor() {
        this.owner = { 'A': 0, 'B': 0, 'C': 0 };
        this.uid = "";
        this.tester = "";
        this.uidRefreshToken = "";
        this.fcmToken = "";
        this.second_user_name = "";
        this.email = "";
        this.user_name = "";
        this.phone = "";
        this.shop = "";
        this.team = "";
        this.rating = "";
        this.company = "";
        this.group_token = "";
        this.seed_selected = false;
    }
}
exports.UserItem = UserItem;
//# sourceMappingURL=UserItem.js.map