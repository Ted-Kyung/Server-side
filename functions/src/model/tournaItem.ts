import { RoundRobinModel } from './roundRobin';
import { KnockOutModel } from './knockOut';
import { DoubleEliModel } from './doubleEli';
import { DoubleEliFinalModel } from './doubleEliFinal';

export class TournaItem {
  key: string;
  key_finish: string;
  tourna_name: string;
  tourna_date: string;
  tourna_time : string;
  tourna_where_shop_name: string;
  tourna_where_shop_addr: string;
  tourna_cost: string;
  tourna_many: string;
  tourna_first_price: string;
  tourna_second_price: string;
  tourna_third_price: string;
  tourna_phone_major: string;
  tourna_phone_minor: string;
  tourna_bank: string;
  tourna_bank_addr: string;
  tourna_type:string;
  tourna_accout: string;
  tourna_state: string;
  join_user:Map<string, string>;
  join_user_len:Number;
  round_robin:Map<string, RoundRobinModel>;
  knock_out:Map<string, KnockOutModel>;
  double_eli:Map<string, DoubleEliModel>;
  double_eli_final:Map<string, DoubleEliFinalModel>;
  owner_id: string;
  is_single_elimination: boolean;
  champion: string;
  setUserResultHistory: boolean;
  handicap:boolean;
  roundRobinType:string;
  knockOutType:string;

  constructor() {
    this.key = "";
    this.key_finish = "";
    this.tourna_name = "";
    this.tourna_date = "";
    this.tourna_time  = "";
    this.tourna_where_shop_name = "";
    this.tourna_where_shop_addr = "";
    this.tourna_cost = "";
    this.tourna_many = "";
    this.tourna_type = "";
    this.tourna_first_price = "";
    this.tourna_second_price = "";
    this.tourna_third_price = "";
    this.tourna_phone_major = "";
    this.tourna_phone_minor = "";
    this.tourna_bank = "";
    this.tourna_bank_addr = "";
    this.tourna_accout = "";
    this.tourna_state = "";
    this.owner_id = "";
    this.handicap=false;
    this.roundRobinType="";
    this.knockOutType="";
    this.join_user = new Map<string,  string>();
    this.join_user_len = 0;
    this.round_robin = new Map<string, RoundRobinModel>();
    this.knock_out = new Map<string, KnockOutModel>();
    this.double_eli = new Map<string, DoubleEliModel>();
    this.double_eli_final = new Map<string, DoubleEliFinalModel>();
  }
}