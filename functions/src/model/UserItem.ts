export class UserItem {
  uid: string;
  uidRefreshToken: string;
  fcmToken: string;
  tester:string;
  email: string;
  user_name: string;
  second_user_name: string;
  phone: string;
  shop: string;
  team: string;
  rating: string;
  company: string;
  group_token: string;
  tournaments_token: Array<any>;
  owner = {'A':0, 'B': 0, 'C':0};
  seed_selected: boolean;
  round_robin_number: number;
  tournamentation_number: number;
  match_participation: number;
  match_knockout_participation: number;
  match_semifinal_participation: number;
  match_final_participation: number;
  match_champion: number;
  match_win_by: Array<any>;
  match_lose_by: Array<any>;


  constructor() {
    this.uid = "";
    this.tester = "";
    this.uidRefreshToken = "";
    this.fcmToken = "";
    this.second_user_name = "";
    this.email = "";
    this.user_name = "";
    this.phone = "";
    this.shop = "";
    this.team = "";
    this.rating = "";
    this.company = "";
    this.group_token = "";
    this.seed_selected = false;
  }
}