class resultDefinition {
  public collocate: boolean;
  public collocateInfo: string;
  public userKey: string;
  public score: number;
}

export class KnockOutModel {
    public knockOutSchedule: Array<resultDefinition>
}