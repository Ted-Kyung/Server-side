class RoundRobinDefinition {
  public user1: string;
  public user2: string;
  public result: string;
}

class resultDefinition {
  public userKey: string;
  public winGameCount: number;
  public loseGameCount: number;
  public winCount: number;
  public loseCount: number;
  public drawCount: number;
  public winningMember: Array<any>;
  public score: number;
}

export class RoundRobinModel {
  public winnerCount: number;
  public groupNumber: number;
  // Play board number
  public boardNumber:number;
  // Todo : list up user ids
  public attendedMembers: Array<any>;
  // Todo : make game schedule { 1: [user1, user2, result], 2: [user1, user2, result], 3: [user1, user2, result], 4: [user1, user, result], }
  public scheduler: Array<RoundRobinDefinition>;
  public currentScheduleNumber:number;
  public scheduleMaxNumber:number;
  public scheduleProgress:number;
  public result: Array<resultDefinition>;
  public winners: Array<any>;

  constructor() {
    this.winnerCount = 0;
    this.boardNumber = 0;
    this.attendedMembers = [];
    this.currentScheduleNumber = 0;
    this.scheduleMaxNumber = 0;
    this.scheduleProgress = 0;
    this.result = [];
    this.winners = [];
  }
}

