class resultDefinition {
  public collocate: boolean;
  public collocateInfo: string;
  public userKey: string;
  public score: number;
}

export class DoubleEliFinalModel {
    public doubleEliFinalSchedule: Array<resultDefinition>
}