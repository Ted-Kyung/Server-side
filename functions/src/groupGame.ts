import {TournaItem} from './model/tournaItem';
import { resolve } from 'path';

export class GroupGameModule {

  MATCH_INFO = {
    REVISE: 'revise',
    INPUT: 'input'
  };

  REVISE_RESULT_ARROW = {
    UP: 'up',
    DOWN: 'down'
  };

  RESULT_OPTION = {
    WIN: 'win',
    LOSE: 'lose'
  };
  admin = null;
  code = 'test';

  constructor(admin) {
    this.admin = admin;
  }

  // Path
  ROOT_FIREBASE_TUORNAMENTS_BASE_PATH = '/tournamentsItems/';
  ROOT_FIREBASE_FINISHED_TUORNAMENTS_BASE_PATH = '/finishedTournamentsItems/';
  ROOT_FIREBASE_USERITEM_BASE_PATH = '/userItems/';
  JOIN_USER_PATH = '/join_user/';
  GROUP_BASE_PATH = '/round_robin/';
  KNOCK_OUT_BASE_PATH = '/knock_out/';
  DOUBLE_ELI_BASE_PATH = '/double_eli/';
  DOUBLE_ELI_FINAL_BASE_PATH = '/double_eli_final/';

  matchData: any;
  scheduler: any;
  attendedMembers: any;
  attendedMembersMap: any;
  attendedMembersUids: any;
  currentScheduleNumber: any;
  groupInfo: any;
  groupKey: any;
  result: any;
  winnerCount: any;
  tournaItem: TournaItem;
  tournaItemKey: any;

  loadUserItems(uids:Array<any>):Promise<any>{
    return new Promise((resolve, reject)=>{
      var userItems = [];
      this.attendedMembersMap = {};
      uids.forEach(uid=>{
        userItems.push(this.admin.database().ref(`/userItems/${uid}/`).once(`value`))
      });
      Promise.all(userItems).then(res=>{
        var userItemValues = [];
        res.forEach(item=>{
          this.attendedMembersMap[item.val().uid] = item.val();
          userItemValues.push(item.val());
        });
        resolve(userItemValues);
      });
    });
  }

  receiveData(code, requestParams):Promise<any> {
    return new Promise((resolve, reject)=>{
      this.code = code;
      this.groupKey = requestParams.data.groupKey;             // required
      this.tournaItemKey = requestParams.data.tournaItemKey;     // required 
      // get tournaments Item
      let tournaPath = this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH;
      let keyPath = this.tournaItemKey;
      var prms = this.admin.database().ref(`/${code}${tournaPath}${keyPath}`).once(`value`);
      Promise.all([prms]).then(res=>{
        if(res){
          var prms2 = null;
          this.tournaItem = res[0].val();
          this.scheduler = this.tournaItem.round_robin[this.groupKey].scheduler;             // tournaItem/round_robin/{key}/scheduler ->groupKey 
          this.attendedMembersUids = this.tournaItem.round_robin[this.groupKey].attendedMembers;             // tournaItem/round_robin/{key}/attendedMemebers -> groupKey
          this.currentScheduleNumber = this.tournaItem.round_robin[this.groupKey].currentScheduleNumber;             // tournaItem/round_robin/{key}/currentScheduleNumber
          this.groupInfo = this.tournaItem.round_robin[this.groupKey];             // tournaItem/round_robin/{key}
          this.result = this.tournaItem.round_robin[this.groupKey].result;             // tournaItem/round_robin/{key}/result
          this.winnerCount = this.tournaItem.round_robin[this.groupKey].winnerCount;  // tournaItem/round_robin/{key}/winnerCount
          this.loadUserItems(this.attendedMembersUids).then((items)=>{
            this.attendedMembers = items;
            console.log("**** setup code : " + this.code);
            if (requestParams.inputOption === this.MATCH_INFO.INPUT) {
              console.log("**** do _reviseScheduler");
              this._reviseScheduler(this.attendedMembers[requestParams.leftNum], this.attendedMembers[requestParams.rightNum], requestParams.leftScore, requestParams.rightScore)
                .then(res => { resolve(res); }).catch(err => { reject(err); });
            } else if (requestParams.inputOption === this.MATCH_INFO.REVISE) {
              console.log("**** do _reviseByOwner");
              this._reviseByOwner(this.attendedMembers[requestParams.leftNum], this.attendedMembers[requestParams.rightNum], requestParams.leftScore, requestParams.rightScore, requestParams.leftNum, requestParams.rightNum)
                .then(res => { resolve(res); }).catch(err => { reject(err); });
            } else {
              console.log("**** Else");
              reject("else");
            }
          });
        } else {
          reject('not found tournament');
        }
      });
    });
  }

  // hyunt: comment out
  // _setScoreToTable(leftNum, rightNum, leftScore, rightScore) {
  //   this.matchData[leftNum][rightNum].row = leftScore;
  //   this.matchData[leftNum][rightNum].column = rightScore;

  //   // Same with opposite side
  //   this.matchData[rightNum][leftNum].row = rightScore;
  //   this.matchData[rightNum][leftNum].column = leftScore;
  // }

  _reviseScheduler(rowUser, columnUser, rowScore, columnScore) :Promise<any>{
    return new Promise((resolve, reject) => {
      console.log("Input schedule: PARTICIPATION");
      var nowSchedule = this.scheduler[this.currentScheduleNumber],
        result = '';

      var userOne = this.attendedMembers[this.attendedMembersUids.indexOf(nowSchedule.user1)],
        userTwo = this.attendedMembers[this.attendedMembersUids.indexOf(nowSchedule.user2)];

      var userOneScore = null,
        userTwoScore = null;

      var schedulerLength = this.scheduler.length,
        lastScheduleNumber = parseInt(schedulerLength) - 1;

      var scheduleFinished = false;

      var resultInfo = {};

      if (this._isRowColumnData(rowUser.uid, columnUser.uid, userOne.uid, userTwo.uid)) {
        result = rowScore + ':' + columnScore;
      } else if (this._isColumnRowData(rowUser.uid, columnUser.uid, userOne.uid, userTwo.uid)) {
        result = columnScore + ':' + rowScore;
      }
      this.scheduler[this.currentScheduleNumber].result = result;

      userOneScore = result.split(':')[0];
      userTwoScore = result.split(':')[1];

      resultInfo = this._setMatchResult(userOneScore, userTwoScore, userOne, userTwo);
      this._calculatePoint(resultInfo);
      this._setScheduleProgress();

      if (parseInt(this.currentScheduleNumber) >= lastScheduleNumber) {
        this.groupInfo.scheduleProgress = 100.0;
        this.groupInfo.winners = this._getWinners();
        scheduleFinished = true;
      } else {
        this.currentScheduleNumber++;
      }

      this._setGroupData().then((data) => {
        if (scheduleFinished) {
          console.log("scheduleFinished, try set up KnockOut");
          // _setKnockOutMember parameter: revise collocate
          this._setKnockOutMember(false)
          .then((data) => {
            // _showFinishedMatchAlarm parameter: move to main page
            console.log("TODO : _showFinishedMatchAlarm");
            // 앱에서 받아서 finish면 알람 띄울것 
            resolve('finish');
            // this._showFinishedMatchAlarm(true);
          })
          .catch((err)=>{
            console.log("setup KnockOut failed : " + err);
            reject(err);
          });
        } else {
          console.log("schedule not Finished");
          resolve('progress');
        }
      });
    });
  }

  _reviseByOwner(rowUser, columnUser, rowScore, columnScore, leftNum, rightNum):Promise<any> {
    return new Promise((resolve, reject)=>{
      console.log("Input schedule: OWNER");
      let result = '',
          selectedSchedule = null,
          selectedIndex = null;
      let resultInfo = {};

      let isNeedReviseRelativeTotal = false;

      let beforeResult = '',
          beforeResultLeft = '',
          beforeResultRight = '';

      let nowResultLeft = '',
          nowResultRight = '';

      for (let i=0, len=this.scheduler.length; i<len; i++) {
        if (this._isRowColumnData(rowUser.uid, columnUser.uid, this.scheduler[i].user1, this.scheduler[i].user2)) {
          nowResultLeft = rowScore;
          nowResultRight = columnScore;

          result = rowScore + ':' + columnScore;
          selectedIndex = i;
          break;
        } else if (this._isColumnRowData(rowUser.uid, columnUser.uid, this.scheduler[i].user1, this.scheduler[i].user2)) {
          nowResultLeft = columnScore;
          nowResultRight = rowScore;

          result = columnScore + ':' + rowScore;
          selectedIndex = i;
          break;
        }
      }

      selectedSchedule = this.scheduler[selectedIndex];

      beforeResult = selectedSchedule.result;
      beforeResultLeft = beforeResult.split(':')[0];
      beforeResultRight = beforeResult.split(':')[1];

      // App에서만 띄우고 서버로 안보내게 하면 될것 같음 
      // if (parseInt(selectedIndex) > parseInt(this.currentScheduleNumber)) {
      //   // TO DO: ERROR TO CLIENT
      //   // 진행되지 않은 경기는 수정할 수 없다는 에러
      //   console.log("TODO : _showNotYetProgress");
      //   // this._showNotYetProgress();
      //   this._setScoreToTable(leftNum, rightNum, 0, 0);
      //   return;
      // } else {
      //   if (parseInt(nowResultLeft) > parseInt(nowResultRight)) {
      //     if (parseInt(beforeResultLeft) < parseInt(beforeResultRight)) {
      //       isNeedReviseRelativeTotal = true;
      //     }
      //   } else {
      //     if (parseInt(beforeResultLeft) > parseInt(beforeResultRight)) {
      //       isNeedReviseRelativeTotal = true;
      //     }
      //   }
      // }

      this.scheduler[selectedIndex].result = result;
      resultInfo = this._setMatchResult(rowScore, columnScore, rowUser, columnUser);

      if (isNeedReviseRelativeTotal) {
        this._reviseCalculatePoint(resultInfo);
      }
      this._reviseLegPoint();

      if (this._isFinished()) {
        this.groupInfo.winners = this._getWinners();
      }

      this._setGroupData()
      .then((data) => {
        // TO DO: 수정한 다음 새롭게 녹아웃 멤버 셋팅
        if (this._isFinished()) {
          // _setKnockOutMember parameter: revise collocate
          this._setKnockOutMember(true);
        }
        resolve(data);
      })
      .catch((err)=>{
        reject(err);
      });
    });
  }

  _isRowColumnData(rowUserId, columnUserId, userOneId, userTwoId) {
    return rowUserId === userOneId && columnUserId === userTwoId;
  }

  _isColumnRowData(rowUserId, columnUserId, userOneId, userTwoId) {
    return columnUserId === userOneId && rowUserId === userTwoId;
  }

  _setMatchResult(userOneScore, userTwoScore, userOne, userTwo) {
    var winnerId = null,
        loserId = null;

    var userOneId = userOne.uid,
        userTwoId = userTwo.uid;

    if (parseInt(userOneScore) > parseInt(userTwoScore)) {
      winnerId = userOneId;
      loserId = userTwoId;
    } else if (parseInt(userOneScore) < parseInt(userTwoScore)) {
      winnerId = userTwoId;
      loserId = userOneId;
    }

    return {
      winnerId: winnerId,
      loserId: loserId
    };
  }

  _calculatePoint(data) {
    var winnerId = data.winnerId,
        loserId = data.loserId
    for (var i=0, len=this.result.length; i<len; i++) {
      if (this.result[i].userKey === winnerId) {
        if (!!!this.result[i].winningMember) {
          this.result[i].winningMember = [];
        }
        this.result[i].winningMember.push(loserId);

        if (!this.isExistData(this.result[i].winGameCount)) {
          this.result[i].winGameCount = 0;
        }
        this.result[i].winGameCount++;
      } else if (this.result[i].userKey === loserId) {
        if (!this.isExistData(this.result[i].loseGameCount)) {
          this.result[i].loseGameCount = 0;
        }

        this.result[i].loseGameCount++;
      }
      this._getLegPoint(i);
    }
    this._sortMatchList();
  }

  _reviseCalculatePoint(data) {
    var winnerId = data.winnerId,
        loserId = data.loserId
    for (var i=0, len=this.result.length; i<len; i++) {
      if (this.result[i].userKey === winnerId) {
        if (!!!this.result[i].winningMember) {
          this.result[i].winningMember = [];
        }
        this.result[i].winningMember.push(loserId);

        if (!this.isExistData(this.result[i].winGameCount)) {
          this.result[i].winGameCount = 0;
        }
        if (!this.isExistData(this.result[i].loseGameCount)) {
          this.result[i].loseGameCount = 0;
        }

        this.result[i].winGameCount++;
        if (this.result[i] !== 0) {
          this.result[i].loseGameCount--;
        }
      } else if (this.result[i].userKey === loserId) {
        var loseManWinnersList = this.result[i].winningMember;
        for (var j=0, winnersLength=loseManWinnersList.length; j<winnersLength; j++) {
          if (loseManWinnersList[j] === winnerId) {
            this.result[i].winningMember.splice(j, 1);
            break;
          }
        }
        if (!this.isExistData(this.result[i].loseGameCount)) {
          this.result[i].loseGameCount = 0;
        }
        if (!this.isExistData(this.result[i].winGameCount)) {
          this.result[i].winGameCount = 0;
        }

        this.result[i].loseGameCount++;
        if (this.result[i] !== 0) {
          this.result[i].winGameCount--;
        }
      }
    }
  }

  _reviseLegPoint() {
    for (var i=0, len=this.result.length; i<len; i++) {
      this._getLegPoint(i);
    }
    this._sortMatchList();
  }

  isExistData(data) {
    return !(data === null || data === undefined);
  }

  _getLegPoint(resultIndex) {
    var userId = this.result[resultIndex].userKey;
    var resultInfo = null;
    var winRegPoint = 0,
        loseRegPoint = 0;

    for (var i=0, len=this.scheduler.length; i<len; i++) {
      resultInfo = this.scheduler[i].result.split(':');
      if (userId === this.scheduler[i].user1) {
        winRegPoint = winRegPoint + parseInt(resultInfo[0]);
        loseRegPoint = loseRegPoint + parseInt(resultInfo[1]);
      } else if (userId === this.scheduler[i].user2) {
        winRegPoint = winRegPoint + parseInt(resultInfo[1]);
        loseRegPoint = loseRegPoint + parseInt(resultInfo[0]);
      }
    }

    this.result[resultIndex].winCount = winRegPoint;
    this.result[resultIndex].loseCount = loseRegPoint;
    this.result[resultIndex].score = winRegPoint - loseRegPoint;
  }

  _sortMatchList() {
    this.result.sort(function(a, b) {
      return b.score - a.score
    });

    if (this._isFinished()) {
      console.log('Math is finished so set winner win');
      this._setWinnerWin();
    }
  }

  _isFinished() {
    if (!!this.groupInfo.winners) {
      if(this.groupInfo.winners.length == this.winnerCount){
        return true;
      }
    }
    return false;
  }

  _setWinnerWin() {
    var sameScoreUserOne = null,
        sameScoreUserTwo = null;

    var sameScoreList = this._getSameScorer();

    for (var i=0, len=sameScoreList.length; i<len; i++) {
      if (sameScoreList[i].length !== 2) {
        break;
      }
      sameScoreUserOne = sameScoreList[i][0];
      sameScoreUserTwo = sameScoreList[i][1];

      if (sameScoreUserOne.winningMember.includes(sameScoreUserTwo.userKey)) {
        console.log("User one is win by user two.");
        this._applyWinnerWinResult(sameScoreUserOne.userKey, sameScoreUserTwo.userKey);
      } else {
        console.log("User two is win by user one.");
        this._applyWinnerWinResult(sameScoreUserTwo.userKey, sameScoreUserOne.userKey);
      }
    }
  }

  _getSameScorer() {
    var score = 0,
        sameScoreList = [],
        sameScoreIndex = 0;

    for (var i=0, len=this.result.length; i<len; i++) {
      if (i === 0) {
        score = this.result[i].score;
        sameScoreList[sameScoreIndex] = [this.result[i]];
      } else {
        if (score === this.result[i].score) {
           sameScoreList[sameScoreIndex].push(this.result[i])
        } else {
          score = this.result[i].score;
          sameScoreIndex ++;
          sameScoreList[sameScoreIndex] = [this.result[i]];
        }
      }
    }

    return sameScoreList;
  }

  _applyWinnerWinResult(winnerId, loserId) {
    var tempUser = null;

    var winnerResultIndex = null,
        loserResultIndex = null;

    for (var i=0, len=this.result.length; i<len; i++) {
      if (this.result[i].userKey === winnerId) {
        winnerResultIndex = i;
      }

      if (this.result[i].userKey === loserId) {
        loserResultIndex = i;
      }
    }

    if (loserResultIndex < winnerResultIndex) {
      tempUser = this.result[loserResultIndex];
      this.result[loserResultIndex] = this.result[winnerResultIndex];
      this.result[winnerResultIndex] = tempUser;
    }
  }

  _setScheduleProgress() {
    this.groupInfo.scheduleProgress = ((this.groupInfo.currentScheduleNumber + 1) / (this.groupInfo.scheduleMaxNumber)) * 100;
    this.groupInfo.scheduleProgress = Number(this.groupInfo.scheduleProgress.toPrecision(1));
  }


  _getWinners() {
    var winners = [];
    for (var i=0; i<this.winnerCount; i++) {
      winners.push(this.result[i].userKey);
    }
    return winners;
  }

  _getRegisterKnockOutPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.KNOCK_OUT_BASE_PATH;
  }

  _saveKnockOutMember(knockOutKey, knockOutSchedule, resolve, reject) {
    console.log("_saveKnockOutMember");
    let path = this._getRegisterKnockOutPath(this.tournaItem.key) + knockOutKey + '/';
    var prms = this.admin.database().ref(`${path}`).set(knockOutSchedule);
    return Promise.all([prms]).then((respond) => {
      console.log("_saveKnockOutMember success");
      resolve();
    }).catch(err=>{
      console.log("_saveKnockOutMember failed:" + err);
      reject(err);
    });
  }

  _setKnockOutMember(reviseCollocate) {
    return new Promise((resolve, reject) => {
      var knockOutKey = Object.keys(this.tournaItem.knock_out)[0],
          knockOutSchedule = this._getCollocatedKnockOut(knockOutKey, reviseCollocate);
      this._saveKnockOutMember(knockOutKey, knockOutSchedule, resolve, reject);
    });
  }

  _getRegisterRoundRobinPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.GROUP_BASE_PATH;
  }

  _setGroupData() {
    return new Promise((resolve, reject) => {
      this._updateGroupInfo();
      console.log("updateRoundRobinItemWithKey");
      let path = this._getRegisterRoundRobinPath(this.tournaItem.key) + this.groupKey;
      var prms = this.admin.database().ref(`${path}`).set(this.groupInfo);
      return Promise.all([prms]).then(res=>{
        resolve(); 
      });
    });
  }

  _updateGroupInfo() {
    this.groupInfo.attendedMembers = this.attendedMembersUids;
    this.groupInfo.scheduler = this.scheduler;
    this.groupInfo.result = this.result;
    this.groupInfo.currentScheduleNumber = this.currentScheduleNumber;
  }

  _getCollocatedKnockOut(knockOutKey, reviseCollocate) {
    var knockOutSchedule = this.tournaItem.knock_out[knockOutKey];
    var collocateInfo = null;
    var winners = this.groupInfo.winners;

    for (var i=0, roundLen=knockOutSchedule.length; i < roundLen; i++) {
      for (var j=0, matchLen=knockOutSchedule[i].length; j < matchLen; j++) {
        if ((reviseCollocate && knockOutSchedule[i][j].collocate === true) ||
            (!reviseCollocate && knockOutSchedule[i][j].collocate === false)) {
            for (var rank = 1, len=winners.length; rank <= len; rank ++) {
              collocateInfo = knockOutSchedule[i][j].collocateInfo.split('/');
              if (parseInt(collocateInfo[0]) === this.groupInfo.groupNumber &&
                  parseInt(collocateInfo[1]) === rank) {
                knockOutSchedule[i][j].collocate = true;
                knockOutSchedule[i][j].userKey = winners[rank - 1];
                knockOutSchedule[i][j].winningMember = [];
                knockOutSchedule[i][j].score = 0;
                knockOutSchedule[i][j].name = this.attendedMembersMap[winners[rank - 1]].user_name;
                knockOutSchedule[i][j].email = this.attendedMembersMap[winners[rank - 1]].email;
              }
            }
        }
      }
    }
    return knockOutSchedule;
  }

  clickReviseResultButton(code, resultIndex, arrow): Promise<any> {
    return new Promise((resolve, reject)=>{
      var tempResult = null;
      var reviseResultIndex = null;
      if (arrow === this.REVISE_RESULT_ARROW.UP) {
        reviseResultIndex = resultIndex - 1;
      } else if (arrow === this.REVISE_RESULT_ARROW.DOWN) {
        reviseResultIndex = resultIndex + 1;
      }
      tempResult = this.result[reviseResultIndex];
      this.result[reviseResultIndex] = this.result[resultIndex];
      this.result[resultIndex] = tempResult;
      this.groupInfo.winners = this._getWinners();
      this._setGroupData()
      .then((data) => {
        if (this._isFinished()) {
          // _setKnockOutMember parameter: revise collocate
          this._setKnockOutMember(true)
          .then((res)=>{resolve();})
          .catch((error)=>{reject(error);});
        } else {
          resolve();
        }
      })
      .catch((error)=>{
        reject(error);
      });
    });
  }
}


