import {DoubleEliModel} from './model/doubleEli';
import {DoubleEliFinalModel} from './model/doubleEliFinal';
import {KnockOutModel} from './model/knockOut';
import {RoundRobinModel} from './model/roundRobin';
import {UserItem} from './model/UserItem';
import {TournaItem} from './model/tournaItem';

export class TournamentModule{

  // Path
  ROOT_FIREBASE_TUORNAMENTS_BASE_PATH = '/tournamentsItems/';
  ROOT_FIREBASE_FINISHED_TUORNAMENTS_BASE_PATH = '/finishedTournamentsItems/';
  ROOT_FIREBASE_USERITEM_BASE_PATH = '/userItems/';
  JOIN_USER_PATH = '/join_user/';
  GROUP_BASE_PATH = '/round_robin/';
  KNOCK_OUT_BASE_PATH = '/knock_out/';
  DOUBLE_ELI_BASE_PATH = '/double_eli/';
  DOUBLE_ELI_FINAL_BASE_PATH = '/double_eli_final/';
  // Path end 

  DEMO_GAME = 'demo_game';
  demoUser = {};

  BEFORE_MATCH_DATA = 'MATCH';
  UNEARNED_DATA = 'EMPTY';
  ONLY_KNOCK_OUT_COLLOCATE_TEXT = 'ONLY_KNOCK_OUT';

  MINIMUM_GROUP_MEMBER = 3;

  // Number of groups
  groupCount: any;
  // Number of member in each group
  groupMemberCount: any;
  // Number of winner each group
  winningCount: any;
  // Tournament round count
  roundCount: any;
  tournamentItem: TournaItem;
  // Group Leauge attend member
  attendMembers: any;

  // Knock out schedule
  knockOutSchedule: any;
  // Mock knock out member data
  // because in this page knock out does not start
  knockOutData: any;
  // Flag about this knock out is single/double
  isSingleElimination: any;

  isOnlyKnockOut: any;
  groupMemberList: any;

  tournaItem: TournaItem;
  user: UserItem;

  admin = null;
  code = 'test';

  constructor(admin){
    this.admin = admin;
  }

  _getRegisterRoundRobinPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.GROUP_BASE_PATH;
  }

  _setElements(cd, ele):Promise<any>{
    return new Promise((resolve)=>{
      this.code = cd;
      this.groupCount = ele.groupCount;
      this.groupMemberCount = ele.groupMemberCount;
      this.winningCount = ele.winningCount;
      this.roundCount = ele.roundCount;
      this.tournamentItem = ele.tournamentItem;
      this.attendMembers = ele.attendMembers;
      this.isOnlyKnockOut = ele.isOnlyKnockOut;
      this.tournaItem = ele.tournaItem;
      this.isSingleElimination = ele.isSingleElimination;
      this.user = ele.user;
      console.log("**** setup code : " + this.code);
      this._setGroupMember();
      resolve();
    });
  }

  _getRandomNumber(range) {
    return Math.floor((Math.random() * range));
  }

  _setMemberBySeed() {
    var topSeedMember = [],
        normalMember = [];

    for (var i=0, len=this.attendMembers.length; i<len; i++) {
      if (this.attendMembers[i].seed_selected) {
        topSeedMember.push(this.attendMembers[i]);
      } else {
        normalMember.push(this.attendMembers[i]);
      }
    }
    return {
      topSeed: topSeedMember,
      normal: normalMember
    }
  }

  _setGroupMember() {
    var memberData = this._setMemberBySeed(),
        topSeed = memberData.topSeed,
        normal = memberData.normal;

    var groupList = [],
        groupListIndex = 0;


    var selectedIndex = 0;

    for (var z=0; z<this.groupCount; z++) {
      groupList[z] = [];
    }

    for (var i=0, len=topSeed.length; i<len; i++) {
      selectedIndex = this._getRandomNumber(topSeed.length);
      groupList[groupListIndex].push(topSeed[selectedIndex]);
      topSeed.splice(selectedIndex, 1);
      groupListIndex ++;
      if (groupListIndex === this.groupCount || topSeed.length === 0) {
        //Finish to set top seed each group
        break;
      }
    }

    // Concat to one list
    // Set remain member
    var remainMemberList = topSeed.concat(normal);

    for (var y=0; y<this.groupCount; y++) {
      while(groupList[y].length < this.groupMemberCount) {
        selectedIndex = this._getRandomNumber(remainMemberList.length);
        groupList[y].push(remainMemberList[selectedIndex]);
        remainMemberList.splice(selectedIndex, 1);
        if (remainMemberList.length === 0) {
          break;
        }
      }
      if (remainMemberList.length === 0) {
        break;
      }
    }

    this.groupMemberList = groupList;
  }


  setTournamnetFlow(code, elements):Promise<any>{
    return new Promise((resolve, reject)=>{
      this._setElements(code, elements).then(()=>{
        var path = this._getRegisterRoundRobinPath(this.tournaItem.key);
        console.log("**** start set tournament flow !!!");
        this._setToTournaSystem().then((result:any) => {
          // 녹아웃만 진행
          if (this.isOnlyKnockOut) {
            this._createKnockOutStage()
            .then(()=>{
              console.log("**** create only knockout");
              resolve(this.tournaItem.key);
            });
          } else {
            // 그룹리그도 진행
            this._createRountRobinModels(this.groupMemberList).then((result:any) => {
              var respondCount = 0;
              var errChk = 0;
              // TO DO: Add loading
              for (var i=0, len=result.length; i<len; i++) {
                if (result[i].attendedMembers.length < this.MINIMUM_GROUP_MEMBER) {
                  // if error 
                  // this._showAttendedMemberOneAlarm();
                  errChk++;
                  console.warn("**** failed reason: showAttendedMemberOneAlarm");
                  reject('showAttendedMemberOneAlarm');
                } else if (result[i].attendedMembers.length < this.winningCount) {
                  // if error 
                  //this._showWinnerIsMorethanGroupMemberAlarm();
                  errChk++;
                  console.warn("**** failed reason: showWinnerIsMorethanGroupMemberAlarm");
                  reject('showWinnerIsMorethanGroupMemberAlarm');
                }
              }

              var waitPromise = [];
              // TODO !!!! 
              if(errChk == 0){
                for (var i=0, len=result.length; i<len; i++) {
                  waitPromise.push(this.admin.database().ref(`${path}`).push(result[i]));
                }
                Promise.all([waitPromise]).then(()=>{
                  this._createKnockOutStage()
                  .then(()=>{
                    console.log("**** create fully knockout");
                    resolve(this.tournaItem.key);
                  })
                  .catch(()=>{
                    console.warn("**** failed reason: unKnownError");
                    reject('unKnownError');
                  });
                });
              }
              else{
                console.error("errChk over 0");
                reject('unKnownError');
              }
            });
          }
        });
      });
    });
  }

  //토너먼트 구조 잡는 친구
  //파이어베이스에 토너먼트 모델을 적용 시킨다.
  _getTournamentPath() {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH;
  }
  _setToTournaSystem() {
    return new Promise((resolve, reject) => {
      this.tournamentItem.is_single_elimination = this.isSingleElimination;
      var path = this._getTournamentPath() + this.tournamentItem.key;
      console.log("**** setToTournaSystem : " + path);
      var prms = this.admin.database().ref(`${path}`).update(this.tournamentItem);
      Promise.all([prms]).then(()=>{resolve()});
    });
  }

  //녹아웃 만들기
  _createKnockOutStage() :Promise<any>{
    return new Promise((resolve, reject)=>{
      if (this.isOnlyKnockOut) {
        // 녹아웃만 진행시에 시드를 고려하여 참가자리스트 만들기
        this._setKnockOutAttendedMembersBySeed();
      } else {
        // 참가자 리스트 만들기
        this._setKnockOutAttendedMembers();
      }
      this._makeKnockOutSchedule()
      .then(()=>{
        resolve();
      })
      .catch(()=>{
        reject();
      });
    });
  }

  // 시드 고려한 녹아웃 참가자 배정
  // 녹아웃 데이터에 시드 배정받은 애는 앞에서 부터
  // 아닌 애는 뒤에서 부터 일단 데이터만 넣어준다
  // 스케쥴 및 대전상대는 나중에
  _setKnockOutAttendedMembersBySeed() {
    var topSeedCount = 0;
    this.knockOutData = [];

    for (var i=0, len=this.attendMembers.length; i < len ; i++) {
      if (this.attendMembers[i].seed_selected) {
        this.knockOutData.unshift({
          collocate: true,
          collocateInfo: this.ONLY_KNOCK_OUT_COLLOCATE_TEXT,
          score: 0,
          userKey: this.attendMembers[i].uid
        });
        topSeedCount ++;
      } else {
        this.knockOutData.push({
          collocate: true,
          collocateInfo: this.ONLY_KNOCK_OUT_COLLOCATE_TEXT,
          score: 0,
          userKey: this.attendMembers[i].uid
        });
      }
    }
    // Because top seed member seperated each round
    if (topSeedCount === 0) {
      // 시드 배정이 없는 경우 그룹이 두개로 나눠서 앞뒤로 한명씩 붙여준다.
      this.winningCount = 2;
    } else {
      // 시드가 있는경우 시드받은 멤버가 겹치지 않게 하기 위해
      // 가짜로 각 조별 우승자가 시드 카운트 인척 만들어준다.
      this.winningCount = topSeedCount;
    }
  }

  // 시드 없이 그냥 녹아웃 멤버 데이터 저장
  // collocateInfo에 그룹번호/등수 이런식으로 표시
  _setKnockOutAttendedMembers() {
    var memberIndex = 0;

    this.knockOutData = [];

    for (var i=0; i < this.groupCount; i++) {
      for (var j=1; j <= this.winningCount; j++) {
        this.knockOutData[memberIndex] = {
          collocate: false,
          collocateInfo: i + '/' + j,
          userKey: null,
          winningMember: [],
          score: 0
        };
        memberIndex ++;
      }
    }
  }

  _getRegisterKnockOutPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.KNOCK_OUT_BASE_PATH;
  }

  //녹아웃 스케쥴 생성
  _makeKnockOutSchedule(): Promise<any>{
    return new Promise((resolve, reject)=>{
      var knockOutMembers = null,
          knockOutRoundCount = this._getKnockOutRoundCount(),
          knockOutFirstRoundCount = this._getFirstRoundCount();

      knockOutMembers = this._getKnockOutMemberList(knockOutFirstRoundCount);

      this.knockOutSchedule = [];
      for (var i=0; i<=knockOutRoundCount; i++) {
        this.knockOutSchedule[i] = [];
        for (var j=0; j< Math.pow(2, knockOutRoundCount - i); j++) {
          this.knockOutSchedule[i][j] = this.BEFORE_MATCH_DATA;
          if (i === 0) {
            this.knockOutSchedule[i][j] = knockOutMembers[j];
          } else {
            if (this._isExistBeforeGame(i, j)) {
              if (this._isBeforeKnockOutMatchBothUnEarned(i, j)) {
                this.knockOutSchedule[i][j] = this._genUnearnedData();
              } else if (this._isBeforeKnockOutLeftMatchUnEarned(i, j)) {
                this.knockOutSchedule[i][j] = this.knockOutSchedule[i - 1][j * 2 + 1];
              } else if (this._isBeforeKnockOutRightMatchUnearned(i, j)) {
                this.knockOutSchedule[i][j] = this.knockOutSchedule[i - 1][j * 2];
              }
            }
          }
        }
      }

      var path = this._getRegisterKnockOutPath(this.tournaItem.key);
      var prms = this.admin.database().ref(`${path}`).push(this.knockOutSchedule);
      Promise.all([prms]).then(()=>{
        if (this.isSingleElimination) {
          resolve();
        } else {
          this._setDoubleElimination().then((respond) => {
            resolve();
          }).catch((error) => {
            reject();
          });
        } 
      });
    });
  }

  // 라운드 카운트 생성
  _getKnockOutRoundCount() {
    var numberOfMembers = this.knockOutData.length,
        knockOutRoundCount = 0;

    while(numberOfMembers > 1) {
      numberOfMembers = numberOfMembers / 2;
      knockOutRoundCount ++;
    }

    return knockOutRoundCount;
  }

  // 첫번째 라운드에 매치카운트
  _getFirstRoundCount() {
    return Math.pow(2, this._getKnockOutRoundCount())
  }

  _getKnockOutMemberList(firstRoundCount) {
    var sortedMembers = this._getSortedMemberList(firstRoundCount),
        sortedFlag = 0,
        selectedMember = null,
        knockOutMember = [];

    for (var i=0, len = Math.floor(firstRoundCount / this.winningCount); i<len; i++) {
      var frontIndex = 0,
          lastIndex = this.winningCount - 1,
          indexFlag = 0;
      for (var j=0; j<this.winningCount; j++) {
        if (sortedFlag % 2 === 0) {
          selectedMember = sortedMembers.shift();
        } else {
          selectedMember = sortedMembers.pop();
        }
        if (indexFlag % 2 === 0) {
          knockOutMember[Math.floor(firstRoundCount / this.winningCount) * frontIndex + i] = selectedMember;
          frontIndex ++;
        } else {
          knockOutMember[Math.floor(firstRoundCount / this.winningCount) * lastIndex + i] = selectedMember;
          lastIndex --;
        }
        indexFlag ++;
      }
      sortedFlag ++;
    }

    var diffKnockOutMember = firstRoundCount - knockOutMember.length;

    if (diffKnockOutMember > 0) {
      for (var i=0; i<diffKnockOutMember; i++) {
        knockOutMember.splice(i + this.groupCount, 0, this._genUnearnedData());
      }
    }

    return knockOutMember;
  }

  _getSortedMemberList(firstRoundCount) {
    var tempSortedMembers = this._addUnearnedWin(firstRoundCount);
    var sortedMembers = [],
        sortedFlag = 0,
        sortedFrontIndex = 0,
        sortedLastIndex = tempSortedMembers.length - 1;

    for (var i=0, len = tempSortedMembers.length; i<len; i = i + this.winningCount) {
      if (sortedFlag % 2 === 0) {
        for (var j=0; j<this.winningCount; j++) {
          if (!!tempSortedMembers[i + j]) {
            sortedMembers[sortedFrontIndex] = tempSortedMembers[i + j];
            sortedFrontIndex ++;
          }
         }
      } else {
        for (var j=this.winningCount - 1; j>=0; j--) {
          if (!!tempSortedMembers[i + j]) {
            sortedMembers[sortedLastIndex] = tempSortedMembers[i + j];
            sortedLastIndex --;
          }
        }
      }
      sortedFlag ++;
    }

    return sortedMembers;
  }

  //부전승
  _addUnearnedWin(firstRoundCount) {
    var addedUnearnedWinMembers = [];
    for (var i=0; i<firstRoundCount; i++) {
      if (!!this.knockOutData[i]) {
        addedUnearnedWinMembers[i] = this.knockOutData[i];
      } else {
        addedUnearnedWinMembers[i] = this._genUnearnedData();
      }
    }
    return addedUnearnedWinMembers;
  }

  _genUnearnedData() {
    return this.UNEARNED_DATA;
  }

  _isExistBeforeGame(roundCount, matchCount) {
    return !!this.knockOutSchedule[roundCount - 1][matchCount * 2] &&
           !!this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1];
  }

  _isBeforeKnockOutMatchBothUnEarned(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] === this.UNEARNED_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] === this.UNEARNED_DATA
  }

  _isBeforeKnockOutLeftMatchUnEarned(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] === this.UNEARNED_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] !== this.UNEARNED_DATA
  }

  _isBeforeKnockOutRightMatchUnearned(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] !== this.UNEARNED_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] === this.UNEARNED_DATA
  }

  _getRegisterDoubleEliPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.DOUBLE_ELI_BASE_PATH;
  }

  _setDoubleElimination() {
    return new Promise((resolve, reject) => {
      var firstMatchCount = this.knockOutSchedule[0].length;

      var doubleEliSchedule = [],
          doubleEliCount = firstMatchCount / 2,
          doubleEliRoundCount = 0;

      var originRoundCount = 2;
      var additionalMatchCount = 1;

      if (this._isOnlyOneMatchKnockOut()) {
        doubleEliSchedule[doubleEliRoundCount] = [];
        doubleEliSchedule[doubleEliRoundCount][0] = {
          collocate: false,
          collocateInfo: '1' + '/' + '1'
        }
      } else {
        while(doubleEliCount > 1) {
          // Set knockout
          doubleEliSchedule[doubleEliRoundCount] = [];
          for (var i=0; i<doubleEliCount; i++) {
            if (doubleEliRoundCount === 0) {
              // Set first round unearned data
              if (this.knockOutSchedule[0][i*2] === this.UNEARNED_DATA ||
                  this.knockOutSchedule[0][i*2 + 1] === this.UNEARNED_DATA) {
                doubleEliSchedule[doubleEliRoundCount][i] = this.UNEARNED_DATA;
              } else {
                doubleEliSchedule[doubleEliRoundCount][i] = {
                  collocate: false,
                  collocateInfo: '1' + '/' + (i+1)
                }
              }
            } else {
              doubleEliSchedule[doubleEliRoundCount][i] = this.BEFORE_MATCH_DATA;
            }
          }
          doubleEliRoundCount ++;

          // Set knock out additional frame
          doubleEliSchedule[doubleEliRoundCount] = [];
          for (var j=0; j<doubleEliCount; j++) {
            // In this set looser user when progress origin knock out from round 2
            if (j % 2 === 1) {
              doubleEliSchedule[doubleEliRoundCount][j] = {
                collocate: false,
                collocateInfo: originRoundCount + '/' + additionalMatchCount
              }
              additionalMatchCount ++;
            } else {
              doubleEliSchedule[doubleEliRoundCount][j] = this.BEFORE_MATCH_DATA;
            }
          }
          doubleEliRoundCount ++;
          doubleEliCount = doubleEliCount / 2;

          originRoundCount ++;
          additionalMatchCount = 1;
        }

        doubleEliSchedule[doubleEliRoundCount] = [];
        doubleEliSchedule[doubleEliRoundCount][0] = this.BEFORE_MATCH_DATA;


        doubleEliSchedule = this._setDoubleEliUnEarnedData(doubleEliSchedule);
      }

      this._addDoubleEliFinalData(doubleEliSchedule)
      .then((respond) => {
        var path = this._getRegisterDoubleEliPath(this.tournaItem.key);
        var prms = this.admin.database().ref(`${path}`).push(doubleEliSchedule);
        Promise.all([prms]).then(()=>{
          resolve();
        });
      }).catch((error) => {
        reject();
      })
    });
  }

  _isOnlyOneMatchKnockOut() {
    return this.knockOutSchedule.length - 1 === 1;
  }

  _setDoubleEliUnEarnedData(doubleEliSchedule) {
    for (var i=1, len=doubleEliSchedule.length; i<len; i++) {
      for (var j=0, matchLength=doubleEliSchedule[i].length; j<matchLength; j++) {
        if (doubleEliSchedule[i][j] === this.BEFORE_MATCH_DATA) {
          // All matches not include looser in knockout
          if (i % 2 === 0) {
            if (doubleEliSchedule[i-1][j*2] === this.UNEARNED_DATA &&
                doubleEliSchedule[i-1][j*2 + 1] === this.UNEARNED_DATA) {
              doubleEliSchedule[i][j] = this.UNEARNED_DATA;
            } else if (doubleEliSchedule[i-1][j*2] === this.UNEARNED_DATA) {
              doubleEliSchedule[i][j] = doubleEliSchedule[i-1][j*2 + 1];
            } else if (doubleEliSchedule[i-1][j*2 + 1] === this.UNEARNED_DATA) {
              doubleEliSchedule[i][j] = doubleEliSchedule[i-1][j*2];
            } else {
              // Nothing
            }
          // Some element has looser in knockout
          } else {
            if (doubleEliSchedule[i-1][j] === this.UNEARNED_DATA &&
                doubleEliSchedule[i-1][j + 1] === this.UNEARNED_DATA) {
              doubleEliSchedule[i][j] = this.UNEARNED_DATA;
            } else if (doubleEliSchedule[i-1][j] === this.UNEARNED_DATA) {
              doubleEliSchedule[i][j] = doubleEliSchedule[i-1][j+ 1];
            } else if (doubleEliSchedule[i-1][j+ 1] === this.UNEARNED_DATA) {
              doubleEliSchedule[i][j] = doubleEliSchedule[i-1][j];
            } else {
              // Nothing
            }
          }
        } else if (doubleEliSchedule[i][j] !== this.UNEARNED_DATA) {
          if (i % 2 !== 0) {
            var knockOutMatchInfo = doubleEliSchedule[i][j].collocateInfo.split('/');

            var knockOutRoundCount = knockOutMatchInfo[0],
                knockOutMatchCount = knockOutMatchInfo[1];


            if (this._isBeforeKnockOutLeftMatchUnEarned(knockOutRoundCount, knockOutMatchCount - 1) ||
                this._isBeforeKnockOutRightMatchUnearned(knockOutRoundCount, knockOutMatchCount - 1)) {
              doubleEliSchedule[i][j] = this.UNEARNED_DATA;

            }
          }
        }
      }
    }
    return doubleEliSchedule;
  }

  _getRegisterDoubleEliFinalPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.DOUBLE_ELI_FINAL_BASE_PATH;
  }

  _addDoubleEliFinalData(doubleEliSchedule):Promise<any> {
    return new Promise((resolve, reject) => {
      var doubleEliFinalSchedule = [];

      var knockOutLength = this.knockOutSchedule.length,
          doubleEliLength = doubleEliSchedule.length;

      var lastKnockOutSchedule = this.knockOutSchedule[knockOutLength - 1][0],
          lastDoubleEliSchedule = doubleEliSchedule[doubleEliLength - 1][0];

      doubleEliFinalSchedule[0] = [];
      doubleEliFinalSchedule[1] = [];

      // Both side not unearned
      if (lastKnockOutSchedule !== this.UNEARNED_DATA &&
          lastDoubleEliSchedule !== this.UNEARNED_DATA) {
        doubleEliFinalSchedule[0][0] = this.BEFORE_MATCH_DATA;
        doubleEliFinalSchedule[0][1] = {
          collocate: false,
          collocateInfo: 'final looser'
        }
        doubleEliFinalSchedule[1][0] = this.BEFORE_MATCH_DATA;
      // Knockout side unearned
      } else if (lastKnockOutSchedule === this.UNEARNED_DATA &&
                 lastDoubleEliSchedule !== this.UNEARNED_DATA) {
        doubleEliFinalSchedule[0][0] = lastDoubleEliSchedule;
        doubleEliFinalSchedule[0][1] = this.UNEARNED_DATA;
        doubleEliFinalSchedule[1][0] = doubleEliFinalSchedule[0][0];
      // Double elimination side unearned
      } else if (lastKnockOutSchedule !== this.UNEARNED_DATA &&
                 lastDoubleEliSchedule === this.UNEARNED_DATA) {
        doubleEliFinalSchedule[0][0] = lastKnockOutSchedule;
        doubleEliFinalSchedule[0][1] = this.UNEARNED_DATA;
        doubleEliFinalSchedule[1][0] = doubleEliFinalSchedule[0][0];
      }
      let path = this._getRegisterDoubleEliFinalPath(this.tournaItem.key);
      var prms = this.admin.database().ref(`${path}`).push(doubleEliFinalSchedule);
      Promise.all([prms])
      .then(res=>{
        resolve();
      })
      .catch(error=>{
        reject();
      });
    })
  }

  _createRountRobinModels(groupMemberList:Array<any>) {
    return new Promise((resolve, reject) => {
      console.log("**** _createRountRobinModels : " + groupMemberList);
      var robinModelList = [];
      for (var i=0, len=groupMemberList.length; i<len; i++) {
        this._setAboutNumber(i).then((numberData) => {

          robinModelList.push(this._setRobinModel(numberData, groupMemberList[numberData['index']]));

          if (groupMemberList.length === robinModelList.length) {
            resolve(robinModelList);
          }
        });
      }
    });
  }

  _setAboutNumber(index) {
    return new Promise((resolve, reject) => {
      resolve({
        groupNumber: index,
        boardNumber: index,
        index: index
      });
    });
  }

  _setRobinModel(numberData, groupMember) {
    var robinModel = new RoundRobinModel;

    robinModel['groupNumber'] = Number(numberData.groupNumber);
    robinModel['boardNumber'] = Number(numberData.boardNumber);
    robinModel['attendedMembers'] = this._getAttendedMemberList(groupMember);
    robinModel['result'] = this._genResult(groupMember);
    robinModel['scheduler'] = this._getScheduler(groupMember);
    robinModel['scheduleMaxNumber'] = robinModel.scheduler.length;
    robinModel['winnerCount'] = this.winningCount;


    return robinModel;
  }

  _getAttendedMemberList(groupMember) {
    var attendMemberList = [];

    for (var i=0, len=groupMember.length; i<len; i++) {
      attendMemberList.push(groupMember[i].uid);
    }
    return attendMemberList;
  }

  _genResult(groupMember) {
    var resultList = [];

    for (var i=0, len=groupMember.length; i<len; i++) {
      resultList.push({
        userKey: groupMember[i].uid,
        winGameCount: 0,
        loseGameCount: 0,
        winCount: 0,
        loseCount: 0,
        drawCount: 0,
        score: 0,
        winningMember: []
      });
    }

    return resultList;
  }

  _getScheduler(groupMember): any {
    var schedulerGroupMember = groupMember,
        topMember = [],
        bottomMember = [];

    var matchScheduleList = [],
        matchScheduleCount = 0;

    var blankMember = {uid: 'mock'},
        addMockData = false;

    if (schedulerGroupMember.length % 2 !== 0) {
      addMockData = true;
      schedulerGroupMember.push(blankMember);
    }

    for (var i=0, len=schedulerGroupMember.length; i<len; i++) {
      if (i % 2 === 0) {
        topMember.push(schedulerGroupMember[i]);
      } else {
        bottomMember.push(schedulerGroupMember[i]);
      }
    }

    if (schedulerGroupMember.length < 3) {
      // Remove mock data from groupMember
      if (addMockData) {
        for (var i=0, len=schedulerGroupMember.length; i<len; i++) {
          if (schedulerGroupMember[i].uid === blankMember.uid) {
            schedulerGroupMember.splice(i, 1);
            break;
          }
        }
      }
      return {
          user1: topMember[0].uid,
          user2: bottomMember[0].uid,
          result: '0:0'
      }
    }

    matchScheduleList[0] = {};

    while(bottomMember[0].uid !== matchScheduleList[0].user2) {
      var lastTopMember = topMember[topMember.length - 1],
          firstBottomMember = bottomMember[0];

      var newTopMember = [],
          newBottomMember = [];

      for (i=0, len=topMember.length; i<len; i++) {
        matchScheduleList[matchScheduleCount] = {
          user1: topMember[i].uid,
          user2: bottomMember[i].uid,
          result: '0:0'
        };
        matchScheduleCount ++;
      }

      newTopMember[0] = topMember[0];

      newTopMember[1] = firstBottomMember;
      for (var i=2, topLen=topMember.length; i<topLen; i++) {
        newTopMember[i] = topMember[i-1];
      }

      for (var j=0, bottomLen=bottomMember.length; j<bottomLen-1; j++) {
        newBottomMember[j] = bottomMember[j+1];
      }
      newBottomMember[bottomLen-1] = lastTopMember;

      topMember = newTopMember;
      bottomMember = newBottomMember;
    }

    var removeBlankScheduleList = [],
        removeBlankScheduleCount = 0;

    for(var i=0; i<matchScheduleList.length; i++) {
      if (matchScheduleList[i].user1 !== blankMember.uid &&
          matchScheduleList[i].user2 !== blankMember.uid) {
        removeBlankScheduleList[removeBlankScheduleCount] = matchScheduleList[i];
        removeBlankScheduleCount ++;
      }
    }

    // Remove mock data from groupMember
    if (addMockData) {
      for (var i=0, len=schedulerGroupMember.length; i<len; i++) {
        if (schedulerGroupMember[i].uid === blankMember.uid) {
          schedulerGroupMember.splice(i, 1);
          break;
        }
      }
    }
    return removeBlankScheduleList;
  }
}