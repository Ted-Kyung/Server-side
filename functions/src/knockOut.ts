import {TournaItem} from './model/tournaItem';

export class KnockOutGameModule {

  // Path
  ROOT_FIREBASE_TUORNAMENTS_BASE_PATH = '/tournamentsItems/';
  ROOT_FIREBASE_FINISHED_TUORNAMENTS_BASE_PATH = '/finishedTournamentsItems/';
  ROOT_FIREBASE_USERITEM_BASE_PATH = '/userItems/';
  JOIN_USER_PATH = '/join_user/';
  GROUP_BASE_PATH = '/round_robin/';
  KNOCK_OUT_BASE_PATH = '/knock_out/';
  DOUBLE_ELI_BASE_PATH = '/double_eli/';
  DOUBLE_ELI_FINAL_BASE_PATH = '/double_eli_final/';

  DEFAULT_ROUND_PART_HEIGHT = 6;

  BEFORE_MATCH_DATA = 'MATCH';
  UNEARNED_DATA = 'EMPTY';

  DEMO_GAME = 'demo_game';
  DEMO_USER = 'demo_user';

  KNOCK_OUT_WIDTH = 18;

  SINGLE_ELI = 'singleEli';
  DOUBLE_ELI = 'doubleEli';
  DOUBLE_FINAL = 'doubleFinal';

  MATCH_INFO = {
    REVISE: 'revise',
    INPUT: 'input'
  };

  knockOutSchedule: any;
  doubleEliSchedule: any;
  doubleEliFinalSchedule: any;
  tournaItem: TournaItem;
  matchInfo:any;
  leftScore:any;
  rightScore:any;
  knockOutKey:any;
  doubleEliKey:any;
  doubleEliFinalKey:any;

  admin = null;
  code = 'test';

  constructor(admin) {
    this.admin = admin;
  }

  receiveData(code, requestParams):Promise<any> {
    return new Promise((resolve, reject)=>{
      console.log("knockout set code : " + code + ", knockout setup : " + JSON.stringify(requestParams));
      this.code = code;
      // this.knockOutSchedule = requestParams.data.knockOutSchedule;
      // this.doubleEliSchedule = requestParams.data.doubleEliSchedule;
      // this.doubleEliFinalSchedule = requestParams.data.doubleEliFinalSchedule;
      this.matchInfo =  requestParams.matchInfo;
      this.leftScore =  requestParams.leftScore;
      this.rightScore =  requestParams.rightScore;
      this.knockOutKey = requestParams.knockOutKey;
      this.doubleEliKey = requestParams.doubleEliKey;
      this.doubleEliFinalKey = requestParams.doubleEliFinalKey;

      var tournaPath = this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH;
      var tournaKey = requestParams.tournaKey;
      
      var prms = this.admin.database().ref(`/${code}${tournaPath}${tournaKey}`).once(`value`);
      
      Promise.all([prms]).then((res)=>{
        this.tournaItem = res[0].val();
        this.knockOutSchedule = this.tournaItem.knock_out[this.knockOutKey];
        console.log("this knockout game : " + JSON.stringify(this.knockOutSchedule));
        if (this.tournaItem.hasOwnProperty('double_eli')){
          this.doubleEliSchedule = this.tournaItem.double_eli[this.doubleEliKey];
        }
        if (this.tournaItem.hasOwnProperty('double_eli_final')){
          this.doubleEliFinalSchedule = this.tournaItem.double_eli_final[this.doubleEliFinalKey];
        }
        
        if (this.matchInfo.option === this.SINGLE_ELI) {
          console.log("_setKnockOutSchedule");
          this._setKnockOutSchedule(this.matchInfo, requestParams.leftScore, requestParams.rightScore, this.matchInfo.inputOption)
          .then((res)=>{resolve(res);})
          .catch((err)=>{
            console.error("failed to _setKnockOutSchedule : " + err)
            reject(err);
          });
        } else if (this.matchInfo.option === this.DOUBLE_ELI) {
          console.log("_setDoubleEliSchedule");
          this._setDoubleEliSchedule(this.matchInfo, requestParams.leftScore, requestParams.rightScore, this.matchInfo.inputOption)
          .then((res)=>{resolve(res);})
          .catch((err)=>{
            console.error("failed to _setDoubleEliSchedule: " + err)
            reject(err);
          });
        } else {
          console.log("_setDoubleFinalSchedule");
          this._setDoubleFinalSchedule(this.matchInfo, requestParams.leftScore, requestParams.rightScore, this.matchInfo.inputOption)
          .then((res)=>{resolve(res);})
          .catch((err)=>{
            console.error("failed to _setDoubleFinalSchedule: " + err)
            reject(err);
          });
        }
      });
    });
  }

  _setKnockOutSchedule(matchInfo, leftScore, rightScore, inputOption):Promise<any>{
    return new Promise((resolve, reject)=>{
      var roundCount = matchInfo.roundCount,
      matchCount = matchInfo.matchCount;
      var beforeRoundCount = roundCount - 1,
          beforeLeftMatchCount = matchCount * 2,
          beforeRightMatchCount = matchCount * 2 + 1;
      var winMember = null,
          loseMember = null;
      var isNeedReviseRelativeTotal = false;
      if (inputOption === this.MATCH_INFO.REVISE) {
        var beforeLeftScore = this.knockOutSchedule[beforeRoundCount][beforeLeftMatchCount].score,
            beforeRightScore = this.knockOutSchedule[beforeRoundCount][beforeRightMatchCount].score;

        isNeedReviseRelativeTotal = this._getNeedReviseRelativeTotal(leftScore, rightScore, beforeLeftScore, beforeRightScore);
      }
      this.knockOutSchedule[beforeRoundCount][beforeLeftMatchCount].score = leftScore;
      this.knockOutSchedule[beforeRoundCount][beforeRightMatchCount].score = rightScore;

      if (parseInt(leftScore) > parseInt(rightScore)) {
        winMember = this._genResultMemberData(this.knockOutSchedule[beforeRoundCount][beforeLeftMatchCount]);
        loseMember = this._genResultMemberData(this.knockOutSchedule[beforeRoundCount][beforeRightMatchCount]);
      } else if (parseInt(leftScore) < parseInt(rightScore)) {
        winMember = this._genResultMemberData(this.knockOutSchedule[beforeRoundCount][beforeRightMatchCount]);
        loseMember = this._genResultMemberData(this.knockOutSchedule[beforeRoundCount][beforeLeftMatchCount]);
      }

      this.knockOutSchedule[roundCount][matchCount] = winMember;
      console.log("Before Updated knock out schedule : " + JSON.stringify(this.knockOutSchedule));

      if (inputOption === this.MATCH_INFO.REVISE) {
        if (isNeedReviseRelativeTotal) {
          this._reviseRelativeTotal(winMember.userKey, loseMember.userKey);
        }
      } else {
        this._setRelativeTotal(winMember.userKey, loseMember.userKey);
      }

      this._setDoubleEliMember(roundCount, matchCount + 1, loseMember, winMember, isNeedReviseRelativeTotal)
      .then((respond) => {
        this._updateKnockOutSchedule()
        .then(res=>{resolve(res);})
        .catch(err=>{
          console.error("failed to update Knockout schedule");
          reject(err);
        });
      }).catch((error) => {
        console.error("Has not DoubleEle Knocout");
        reject(error);
      });
    });
  }

  _updateKnockOutSchedule() :Promise<any>{
    return new Promise((resolve, reject)=>{
      for (var i=1, len=this.knockOutSchedule.length; i<len; i++) {
        for (var j=0, roundLen=this.knockOutSchedule[i].length; j<roundLen; j++) {
          if (this._isExistBeforeKnockOutGame(i, j)) {
            if (this._isBeforeKnockOutGameBothUnEarned(i, j)) {
              this.knockOutSchedule[i][j] = this._genUnearnedData();
            } else if (this._isBeforeKnockOutGameOneUnEarned(i, j)) {
              this._setOriginKnockOutData(i, j);
            }
          }
        }
      }
      this._setKnockOutData()
      .then(()=>{resolve();})
      .catch((err)=>{
        console.error("_updateKnockOutSchedule : " + err);
        reject(err);
      });
    });
  }

  _getRegisterKnockOutPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.KNOCK_OUT_BASE_PATH;
  }

  _setKnockOutData(): Promise<any>{
    return new Promise((resolve, reject)=>{
      console.log("TODO : updateKnocOutItemWithKey");
      let path = this._getRegisterKnockOutPath(this.tournaItem.key) + this.knockOutKey + '/';
      this.admin.database().ref(`${path}`).set(this.knockOutSchedule);
      resolve('success');
    });
  }

  _setOriginKnockOutData(roundCount, matchCount) {
    var originScore = 0;

    var beforeRoundCount = roundCount - 1,
        beforeLeftMatchCount = matchCount * 2,
        beforeRightMatchCount = matchCount * 2 + 1;

    if (!!this.knockOutSchedule[roundCount][matchCount].score) {
      originScore = this.knockOutSchedule[roundCount][matchCount].score;
    }

    if (this._isBeforeKnockOutGameLeftUnearned(roundCount, matchCount)) {
      this.knockOutSchedule[roundCount][matchCount] = this._getMatchData(this.knockOutSchedule[beforeRoundCount][beforeRightMatchCount]);
    } else if (this._isBeforeKnockOutGameRightUnearned(roundCount, matchCount)) {
      this.knockOutSchedule[roundCount][matchCount] = this._getMatchData(this.knockOutSchedule[beforeRoundCount][beforeLeftMatchCount]);
    }

    this.knockOutSchedule[roundCount][matchCount].score = originScore;

  }

  _getNeedReviseRelativeTotal(nowLeftScore, nowRightScore, beforeLeftScore, beforeRightScore) {
    if (parseInt(nowLeftScore) > parseInt(nowRightScore)) {
      if (parseInt(beforeLeftScore) < parseInt(beforeRightScore)) {
        return true;
      }
    } else {
      if (parseInt(beforeLeftScore) > parseInt(beforeRightScore)) {
        return true;
      }
    }

    return false;
  }

  _genResultMemberData(member) {
    var memberData =  {
      collocate: member.collocate,
      collocateInfo: member.collocateInfo,
      score: 0,
      userKey: member.userKey,
      name: member.name,
      email: member.email
    };

    return memberData
  }

  setKnockoutResult(winner, loser){
    this.admin.database().ref(`/userItems/${winner}/match_win_by/`).push(loser);
    this.admin.database().ref(`/userItems/${loser}/match_lose_by/`).push(winner);
  }

  _setRelativeTotal(winnerId, loserId):Promise<any> {
    return new Promise((resolve, reject)=>{
      console.log("_setRelativeTotal");
      this.setKnockoutResult(winnerId, loserId);
      resolve();
    });
  }

  reviseWinnerCase(winner, loser, winnersLoserList, winnersLoserListKey){
    // remove once loserById in Winner's Loser List
    winnersLoserListKey.forEach(key=>{
      if(winnersLoserList[key] == loser){
        this.admin.database().ref(`/userItems/${winner}/match_lose_by/${key}/`).remove();
        return;
      }
    });
  }

  reviseLoserCase(winner, loser, losersWinnerList, losersWinnerListKey){
    // remove once winnerById in Loser's Winner List
    losersWinnerListKey.forEach(key2=>{
      if(losersWinnerList[key2] == winner){
        this.admin.database().ref(`/userItems/${loser}/match_win_by/${key2}/`).remove();
        return;
      }
    });
  }

  reviseKnockoutResult(winner, loser):Promise<any>{
    return new Promise((resolve)=>{
      this.admin.database().ref(`/userItems/${winner}/match_win_by/`).push(loser);
      this.admin.database().ref(`/userItems/${loser}/match_lose_by/`).push(winner);
      var loserListOfWinner = this.admin.database().ref(`/userItems/${winner}/match_lose_by/`).once(`value`);
      var winnerListOfLoser = this.admin.database().ref(`/userItems/${loser}/match_win_by/`).once(`value`);
      return Promise.all([winnerListOfLoser, loserListOfWinner]).then(res=>{
        
        var winnersLoserList = res[1].val();
        var winnersLoserListKey = Object.keys(winnersLoserList);
        this.reviseWinnerCase(winner, loser, winnersLoserList, winnersLoserListKey);
        
        var losersWinnerList = res[0].val();
        var losersWinnerListKey = Object.keys(losersWinnerList);
        this.reviseLoserCase(winner, loser, losersWinnerList, losersWinnerListKey);
        resolve();
      });
    });
  }

  _reviseRelativeTotal(winnerId, loserId):Promise<any>{
    return new Promise((resolve, reject)=>{
      console.log("TODO : _reviseRelativeTotal")
      this.reviseKnockoutResult(winnerId, loserId)
      .then(()=>{resolve();});
    });
  }

  _setDoubleEliMember(roundCount, matchCount, member, originMember, isNeedRevise) {
    return new Promise((resolve, reject) => {
      // If not double elimination does not progress
      // Just progress single elimination
      if (this.isSingleElimination()) {
        resolve();
      } else {
        var matchData = null;
        var collocateData = null,
            collocateRound = null,
            collocateMatch = null;

        for (var i=0, roundLen=this.doubleEliSchedule.length; i<roundLen; i++) {
          for (var j=0, matchLen=this.doubleEliSchedule[i].length; j<matchLen; j++) {
            matchData = this.doubleEliSchedule[i][j];
            if (isNeedRevise) {
              if (this.isCollocate(matchData)) {
                if (matchData.userKey === originMember.userKey) {
                  this.doubleEliSchedule[i][j] = member;
                }
              }
            } else {
              if (!this.isBeforeMatch(matchData)) {
                if (!this.isUnEarnedMatch(matchData) && !this.isCollocate(matchData)) {
                  collocateData = this.doubleEliSchedule[i][j].collocateInfo.split('/');
                  collocateRound = collocateData[0];
                  collocateMatch = collocateData[1];
                  if (parseInt(collocateRound) === roundCount && parseInt(collocateMatch) === matchCount) {
                    this.doubleEliSchedule[i][j] = member;
                  }
                }
              }
            }
          }
        }

        this._updateDoubleEliSchedule().then((respond) => {
          resolve();
        }).catch((error) => {
          console.error("Update double eli schedule error", error);
          reject(error);
        });
      }
    })
  }

  isSingleElimination() {
    return !!this.tournaItem.is_single_elimination;
  }

  isCollocate(match) {
    return !!match && !!match.collocate;
  }

  isBeforeMatch(match) {
    return match === this.BEFORE_MATCH_DATA;
  }

  isUnEarnedMatch(match) {
    return match === this.UNEARNED_DATA;
  }

  _updateDoubleEliSchedule() {
    return new Promise((resolve, reject) => {
      for (var i=1, len=this.doubleEliSchedule.length; i<len; i++) {
        for (var j=0, roundLen=this.doubleEliSchedule[i].length; j<roundLen; j++) {
          // All matches not include looser from knockout
          if (!this._hasLoserFromKnockOutRound(i)) {
            if (this._isExistBeforeDoubleEliGame(i, j)) {
              if (this._isBeforeDoubleEliGameBothUnEarned(i, j)) {
                this.doubleEliSchedule[i][j] = this._genUnearnedData();
              } else if (this._isBeforeDoubleEliGameOneUnEarned(i, j)) {
                  this._setOriginDoubleEliData(i, j);
              }
            }

          // Some element has looser from knockout
          } else {
            // Except element from knockout
            if (j % 2 === 0) {
              if (this._isExistBeforeDoubleEliGame(i, j)) {
                if (this._isBeforeDoubleEliGameBothUnEarned(i, j)) {
                  this.doubleEliSchedule[i][j] = this._genUnearnedData();
                } else if (this._isBeforeDoubleEliGameOneUnEarned(i, j)) {
                    this._setOriginDoubleEliData(i, j);
                }
              }
            }
          }
        }
      }
      this._setDoubleEliData()
      .then(()=>{resolve();})
      .catch((error)=>{
        console.error("_updateDoubleEliSchedule");
        reject(error);
      });
    });
  }

  _hasLoserFromKnockOutRound(roundCount) {
    return roundCount % 2 === 1;
  }

  _isExistBeforeDoubleEliGame(roundCount, matchCount) {
    var beforeRoundCount = roundCount - 1,
        beforeLeftMatchCount = null,
        beforeRightMatchCount = null;

    if (this._hasLoserFromKnockOutRound(roundCount)) {
      beforeLeftMatchCount = matchCount;
      beforeRightMatchCount = matchCount + 1;
    } else {
      beforeLeftMatchCount = matchCount * 2;
      beforeRightMatchCount = matchCount * 2 + 1;
    }
    return this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount] !== this.BEFORE_MATCH_DATA &&
           this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount] !== this.BEFORE_MATCH_DATA;
  }

  _isBeforeDoubleEliGameBothUnEarned(roundCount, matchCount) {
    var beforeRoundCount = roundCount - 1,
        beforeLeftMatchCount = null,
        beforeRightMatchCount = null;

    if (this._hasLoserFromKnockOutRound(roundCount)) {
      beforeLeftMatchCount = matchCount;
      beforeRightMatchCount = matchCount + 1;
    } else {
      beforeLeftMatchCount = matchCount * 2;
      beforeRightMatchCount = matchCount * 2 + 1;
    }
    return this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount] === this.UNEARNED_DATA &&
           this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount] === this.UNEARNED_DATA
  }

  _genUnearnedData() {
    return this.UNEARNED_DATA;
  }

  _isBeforeDoubleEliGameOneUnEarned(roundCount, matchCount) {
    return this._isBeforeDoubleEliGameLeftUnearned(roundCount, matchCount) ||
           this._isBeforeDoubleEliGameRightUnearned(roundCount, matchCount);
  }

  _isBeforeDoubleEliGameLeftUnearned(roundCount, matchCount) {
    var beforeRoundCount = roundCount - 1,
        beforeLeftMatchCount = null,
        beforeRightMatchCount = null;

    if (this._hasLoserFromKnockOutRound(roundCount)) {
      beforeLeftMatchCount = matchCount;
      beforeRightMatchCount = matchCount + 1;
    } else {
      beforeLeftMatchCount = matchCount * 2;
      beforeRightMatchCount = matchCount * 2 + 1;
    }
    return this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount] === this.UNEARNED_DATA &&
           this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount] !== this.UNEARNED_DATA
  }

  _isBeforeDoubleEliGameRightUnearned(roundCount, matchCount) {
    var beforeRoundCount = roundCount - 1,
        beforeLeftMatchCount = null,
        beforeRightMatchCount = null;

    if (this._hasLoserFromKnockOutRound(roundCount)) {
      beforeLeftMatchCount = matchCount;
      beforeRightMatchCount = matchCount + 1;
    } else {
      beforeLeftMatchCount = matchCount * 2;
      beforeRightMatchCount = matchCount * 2 + 1;
    }
    return this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount] !== this.UNEARNED_DATA &&
           this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount] === this.UNEARNED_DATA
  }

  _setOriginDoubleEliData(roundCount, matchCount) {
    var originScore = 0;

    var beforeRoundCount = roundCount - 1,
        beforeLeftMatchCount = null,
        beforeRightMatchCount = null;

    if (!!this.doubleEliSchedule[roundCount][matchCount].score) {
      originScore = this.doubleEliSchedule[roundCount][matchCount].score;
    }

    if (this._hasLoserFromKnockOutRound(roundCount)) {
      beforeLeftMatchCount = matchCount;
      beforeRightMatchCount = matchCount + 1;
    } else {
      beforeLeftMatchCount = matchCount * 2;
      beforeRightMatchCount = matchCount * 2 + 1;
    }

    if (this._isBeforeDoubleEliGameLeftUnearned(roundCount, matchCount)) {
      this.doubleEliSchedule[roundCount][matchCount] = this._getMatchData(this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount]);
    } else if (this._isBeforeDoubleEliGameRightUnearned(roundCount, matchCount)) {
      this.doubleEliSchedule[roundCount][matchCount] = this._getMatchData(this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount]);
    }

    this.doubleEliSchedule[roundCount][matchCount].score = originScore;
  }

  _getMatchData(data) {
    var collocate = !!data.collocate ? data.collocate : null,
        collocateInfo = !!data.collocateInfo ? data.collocateInfo : null,
        name = !!data.name ? data.name : null,
        userKey = !!data.userKey ? data.userKey : null,
        score = !!data.score ? data.score : 0,
        email = !!data.email ? data.email : null;

    return {
      collocate: collocate,
      collocateInfo: collocateInfo,
      name: name,
      userKey: userKey,
      score: score,
      email: email
    };
  }
  _getRegisterDoubleEliPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.DOUBLE_ELI_BASE_PATH;
  }

  _setDoubleEliData():Promise<any> {
    return new Promise((resolve, reject)=>{
      console.log("TODO : _setDoubleEliData");
      let path = this._getRegisterDoubleEliPath(this.tournaItem.key) + this.doubleEliKey;
      this.admin.database().ref(`${path}`).set(this.doubleEliSchedule);
      resolve();
    });
  }

  _isExistBeforeKnockOutGame(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] !== this.BEFORE_MATCH_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] !== this.BEFORE_MATCH_DATA;
  }

  _isBeforeKnockOutGameBothUnEarned(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] === this.UNEARNED_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] === this.UNEARNED_DATA
  }

  _isBeforeKnockOutGameLeftUnearned(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] === this.UNEARNED_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] !== this.UNEARNED_DATA
  }

  _isBeforeKnockOutGameRightUnearned(roundCount, matchCount) {
    return this.knockOutSchedule[roundCount - 1][matchCount * 2] !== this.UNEARNED_DATA &&
           this.knockOutSchedule[roundCount - 1][matchCount * 2 + 1] === this.UNEARNED_DATA
  }

  _isBeforeKnockOutGameOneUnEarned(roundCount, matchCount) {
    return this._isBeforeKnockOutGameLeftUnearned(roundCount, matchCount) ||
           this._isBeforeKnockOutGameRightUnearned(roundCount, matchCount);
  }

  _setDoubleEliSchedule(matchInfo, leftScore, rightScore, inputOption):Promise<any> {
    return new Promise((resolve, reject)=>{
      var roundCount = matchInfo.roundCount,
      matchCount = matchInfo.matchCount;
      var beforeRoundCount = roundCount - 1,
          beforeLeftMatchCount = null,
          beforeRightMatchCount = null;

      var winMember = null,
          loseMember = null;

      var isNeedReviseRelativeTotal = false;

      if (!this._hasLoserFromKnockOutRound(roundCount)) {
        beforeLeftMatchCount = matchCount * 2;
        beforeRightMatchCount = matchCount * 2 + 1;
      } else {
        beforeLeftMatchCount = matchCount;
        beforeRightMatchCount = matchCount + 1;
      }

      if (inputOption === this.MATCH_INFO.REVISE) {
        var beforeLeftScore = this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount].score,
            beforeRightScore = this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount].score;

        isNeedReviseRelativeTotal = this._getNeedReviseRelativeTotal(leftScore, rightScore, beforeLeftScore, beforeRightScore);
      }

      this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount].score = leftScore;
      this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount].score = rightScore;

      if (parseInt(leftScore) > parseInt(rightScore)) {
        winMember = this._genResultMemberData(this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount]);
        loseMember = this._genResultMemberData(this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount]);
      } else if (parseInt(leftScore) < parseInt(rightScore)) {
        winMember = this._genResultMemberData(this.doubleEliSchedule[beforeRoundCount][beforeRightMatchCount]);
        loseMember = this._genResultMemberData(this.doubleEliSchedule[beforeRoundCount][beforeLeftMatchCount]);
      }

      this.doubleEliSchedule[roundCount][matchCount] = winMember;

      if (inputOption === this.MATCH_INFO.REVISE) {
        if (isNeedReviseRelativeTotal) {
          this._reviseRelativeTotal(winMember.userKey, loseMember.userKey)
          .then(()=>{}).catch(()=>{});
        }
      } else {
        this._setRelativeTotal(winMember.userKey, loseMember.userKey)
        .then(()=>{}).catch(()=>{});
      }

      this._updateDoubleEliSchedule().then((respond) => {
        resolve();
      }).catch((error) => {
        console.log("_setDoubleEliSchedule error : " + error);
        reject(error);
      });
    });
  }

  _setDoubleFinalSchedule(matchInfo, leftScore, rightScore, inputOption):Promise<any> {
    return new Promise((resolve, reject)=>{
      var roundCount = matchInfo.roundCount;

      var knockOutLastRound = this.knockOutSchedule.length - 1,
          doubleEliLastRound = this.doubleEliSchedule.length - 1;
      var winMember = null,
          loseMember = null;
  
      var isNeedReviseRelativeTotal = false;
  
      if (roundCount === 0) {
        if (inputOption === this.MATCH_INFO.REVISE) {
          var beforeLeftScore = this.knockOutSchedule[knockOutLastRound][0].score,
              beforeRightScore = this.doubleEliSchedule[doubleEliLastRound][0].scor;
  
          isNeedReviseRelativeTotal = this._getNeedReviseRelativeTotal(leftScore, rightScore, beforeLeftScore, beforeRightScore);
        }
  
        this.knockOutSchedule[knockOutLastRound][0].score = leftScore;
        this.doubleEliSchedule[doubleEliLastRound][0].score = rightScore;
  
        if (parseInt(leftScore) > parseInt(rightScore)) {
          winMember = this._genResultMemberData(this.knockOutSchedule[knockOutLastRound][0]);
          loseMember = this._genResultMemberData(this.doubleEliSchedule[doubleEliLastRound][0]);
        } else if (parseInt(leftScore) < parseInt(rightScore)) {
          winMember = this._genResultMemberData(this.doubleEliSchedule[doubleEliLastRound][0]);
          loseMember = this._genResultMemberData(this.knockOutSchedule[knockOutLastRound][0]);
        }
  
        this.doubleEliFinalSchedule[0][0] = winMember;
        this.doubleEliFinalSchedule[0][1] = loseMember;
      } else {
  
        if (inputOption === this.MATCH_INFO.REVISE) {
          var _ifbeforeLeftScore = this.doubleEliFinalSchedule[0][0].score,
              _ifbeforeRightScore = this.doubleEliFinalSchedule[0][1].score;
  
          isNeedReviseRelativeTotal = this._getNeedReviseRelativeTotal(leftScore, rightScore, _ifbeforeLeftScore, _ifbeforeRightScore);
        }
  
        this.doubleEliFinalSchedule[0][0].score = leftScore;
        this.doubleEliFinalSchedule[0][1].score = rightScore;
  
        if (parseInt(leftScore) > parseInt(rightScore)) {
          winMember = this._genResultMemberData(this.doubleEliFinalSchedule[0][0]);
          loseMember = this._genResultMemberData(this.doubleEliFinalSchedule[0][1]);
        } else if (parseInt(leftScore) < parseInt(rightScore)) {
          winMember = this._genResultMemberData(this.doubleEliFinalSchedule[0][1]);
          loseMember = this._genResultMemberData(this.doubleEliFinalSchedule[0][0]);
        }
  
        this.doubleEliFinalSchedule[1][0] = winMember;
      }
  
      if (inputOption === this.MATCH_INFO.REVISE) {
        if (isNeedReviseRelativeTotal) {
          this._reviseRelativeTotal(winMember.userKey, loseMember.userKey);
        }
      } else {
        this._setRelativeTotal(winMember.userKey, loseMember.userKey);
      }
  
      this._updateDoubleEliFinalSchedule()
      .then(()=>{resolve();})
      .catch((error)=>{
        console.error("_setDoubleFinalSchedule");
        reject(error);
      });
    });
  }

  _updateDoubleEliFinalSchedule():Promise<any> {
    return new Promise((resolve, reject)=>{
      if (this._isBeforeDoubeEliFinalGameBothUnEarned()) {
        this.doubleEliFinalSchedule[1][0] = this._genUnearnedData();
      } else if (this._isBeforeDoubleEliFinalOneUnEarned()) {
        this._setOriginDoubleEliFinalData();
      }
      this._setDoubleEliFinalData()
      .then(()=>{resolve();})
      .catch((error)=>{
        console.error("_updateDoubleEliFinalSchedule");
        reject(error);
      });
    });
  }


  _setOriginDoubleEliFinalData() {
    var originScore = 0;

    if (!!this.doubleEliFinalSchedule[1][0].score) {
      originScore = this.doubleEliFinalSchedule[1][0].score;
    }

    if (this._isBeforeDoubleEliFinalLeftUnearned()) {
      this.doubleEliFinalSchedule[1][0] = this._getMatchData(this.doubleEliFinalSchedule[0][1]);
    } else if (this._isBeforeDoubleEliFinalRightUnearned()) {
      this.doubleEliFinalSchedule[1][0] = this._getMatchData(this.doubleEliFinalSchedule[0][0]);
    }

    this.doubleEliFinalSchedule[1][0].score = originScore;
  }

  _getRegisterDoubleEliFinalPath(tId) {
    return '/' + this.code + this.ROOT_FIREBASE_TUORNAMENTS_BASE_PATH + tId + this.DOUBLE_ELI_FINAL_BASE_PATH;
  }

  _setDoubleEliFinalDataToDB():Promise<any>{
    return new Promise((resolve, reject)=>{
      let path = this._getRegisterDoubleEliFinalPath(this.tournaItem.key) + this.doubleEliFinalKey;
      this.admin.database().ref(`${path}`).set(this.doubleEliFinalSchedule);
      resolve();
    });
  }

  _setDoubleEliFinalData():Promise<any>{
    return new Promise((resolve, reject)=>{
      console.log("TODO : _setDoubleEliFinalData")
      var prmsList = [];
      prmsList.push(this._setKnockOutData());
      prmsList.push(this._setDoubleEliData());
      prmsList.push(this._setDoubleEliFinalDataToDB());
      Promise.all(prmsList).then((res)=>{
        resolve();
      });
    });
  }

  _isBeforeDoubeEliFinalGameBothUnEarned() {
    return this.doubleEliFinalSchedule[0][0] === this.UNEARNED_DATA &&
           this.doubleEliFinalSchedule[0][1] === this.UNEARNED_DATA;
  }

  _isBeforeDoubleEliFinalLeftUnearned() {
    return this.doubleEliFinalSchedule[0][0] === this.UNEARNED_DATA &&
           this.doubleEliFinalSchedule[0][1] !== this.UNEARNED_DATA;
  }

  _isBeforeDoubleEliFinalRightUnearned() {
    return this.doubleEliFinalSchedule[0][0] !== this.UNEARNED_DATA &&
           this.doubleEliFinalSchedule[0][1] === this.UNEARNED_DATA;
  }

  _isBeforeDoubleEliFinalOneUnEarned() {
    return this._isBeforeDoubleEliFinalLeftUnearned() ||
           this._isBeforeDoubleEliFinalRightUnearned();
  }
}
